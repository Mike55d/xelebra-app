<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class TipoEventoController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction()
    {
        return $this->render('AdminBundle:TipoEvento:index.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/new")
     */
    public function newAction()
    {
        return $this->render('AdminBundle:TipoEvento:new.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/edit")
     */
    public function editAction()
    {
        return $this->render('AdminBundle:TipoEvento:edit.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/del")
     */
    public function delAction()
    {
        return $this->render('AdminBundle:TipoEvento:del.html.twig', array(
            // ...
        ));
    }

}
