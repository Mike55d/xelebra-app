<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\TipoLista;


/**
     * @Route("/TipoListas")
     */

class TipoListaController extends Controller
{
     /**
     * @Route("/" , name="AdminTipoListas_index")
     */
    public function indexAction()
    {
        $em =$this->getDoctrine()->getManager(); 
        $TipoListas = $em->getRepository('AppBundle:TipoLista')->findAll(); 
        return $this->render('AdminBundle:TipoLista:index.html.twig', array(
            'tipoListas'=> $TipoListas
        ));
    }

    /**
     * @Route("/new" , name="AdminTipoListas_new")
     */
    public function newAction(Request $request ) 
    {
        $em =$this->getDoctrine()->getManager(); 
        if ($request->get('tipo')) {
            $TipoLista = new TipoLista;
            $TipoLista->setTipo($request->get('tipo'));
            $em->persist($TipoLista);
            $em->flush();
            return $this->redirectToRoute('AdminTipoListas_index');
        }
        return $this->render('AdminBundle:TipoLista:new.html.twig', array(
            
        ));
    }

    /**
     * @Route("/{id}/edit" , name="AdminTipoListas_edit")
     */
    public function editAction(TipoLista $TipoLista , Request $request )
    {
        $em =$this->getDoctrine()->getManager(); 
        if ($request->get('tipo')) {
            $TipoLista->setTipo($request->get('tipo'));
            $em->flush();
            return $this->redirectToRoute('AdminTipoListas_index');
        }
        return $this->render('AdminBundle:TipoLista:edit.html.twig', array(
            'tipoLista'=>$TipoLista
        ));
    }

    /**
     * @Route("/{id}/del" , name="AdminTipoListas_del")
     */
    public function delAction(TipoLista $TipoLista)
    {
        $em =$this->getDoctrine()->getManager(); 
        $em->remove($TipoLista);
        $em->flush();
        return $this->redirectToRoute('AdminTipoListas_index');
    }

}
