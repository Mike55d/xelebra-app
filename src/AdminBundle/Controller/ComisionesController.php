<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Comisiones;

/**
     * @Route("comisiones")
     */
class ComisionesController extends Controller
{
    /**
     * @Route("/" , name="AdminComisiones_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager(); 
        $comsiones = $em->getRepository('AppBundle:Comisiones')->findAll(); 
        return $this->render('AdminBundle:Comisiones:index.html.twig', array(
            'comisiones' => $comsiones    
        ));
    }

    /**
     * @Route("/new" , name="AdminComisiones_new")
     */
    public function newAction()
    {
        return $this->render('AdminBundle:Comisiones:new.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/{id}/edit" , name="AdminComisiones_edit")
     */
    public function editAction(Comisiones $comision , Request $request)
    {   
        $em =$this->getDoctrine()->getManager(); 
        if ($request->get('fijo')) {
            $comision->setFijo($request->get('fijo'));
            $comision->setPorcentNovio($request->get('novio'));
            $comision->setPorcentInvitado($request->get('invitado'));
            $comision->setFijoNovio($request->get('fijoNovio'));
            $comision->setFijoInvitado($request->get('fijoInvitado'));
            $comision->setImpuestoPais($request->get('pais'));
            $em->flush();
            return $this->redirectToRoute('AdminComisiones_index');
        }
        return $this->render('AdminBundle:Comisiones:edit.html.twig', array(
           'comision'=>$comision
        ));
    }

}
