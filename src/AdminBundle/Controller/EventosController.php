<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Evento;

/**
* @Route("eventos")
*/
class EventosController extends Controller
{
    /**
     * @Route("/" , name="AdminEventos_index")
     */
    public function indexAction()
    {
        $em =$this->getDoctrine()->getManager(); 
        $eventos = $em->getRepository('AppBundle:Evento')->findAll(); 
        return $this->render('AdminBundle:Eventos:index.html.twig', array(
            'eventos' => $eventos,
        ));
    }

    /**
     * @Route("/{id}/edit" , name="AdminEventos_edit")
     */
    public function editAction(Evento $evento,Request $request)
    {
        $em =$this->getDoctrine()->getManager(); 
        if (!$request->get('check') && $request->get('send')) {
            $evento->setComPersonalizada(0);
            $em->flush();
            return $this->redirectToRoute('AdminEventos_index');
        }
        if ($request->get('fijo')) {
            $evento->setComPersonalizada(1);
            $evento->setFijo($request->get('fijo'));
            $evento->setPorcentNovio($request->get('novio'));
            $evento->setPorcentInvitado($request->get('invitado'));
            $evento->setFijoNovio($request->get('fijoNovio'));
            $evento->setFijoInvitado($request->get('fijoInvitado'));
            $evento->setImpuestoPais($request->get('pais'));
            $em->flush();
            return $this->redirectToRoute('AdminEventos_index');
        }
        return $this->render('AdminBundle:Eventos:edit.html.twig', array(
            'evento' => $evento,
        ));
    }

}
