<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Spipu\Html2Pdf\Html2Pdf;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


/**
 * @Route("/reportes")
 */
class ReportesController extends Controller
{
  /**
  * @Route("/reporteListasNovios" , name="reporteListaNovios")
  */
  public function indexAction(Request $request)
  {
  	$em =$this->getDoctrine()->getManager();
    $tipoEventos = $em->getRepository('AppBundle:tipoEvento')->findAll();
    $paises = $em->getRepository('AppBundle:Pais')->findAll();
    $paisesEvento = [];
    foreach ($paises as $pais) {
      $eventosPais = $em->getRepository('AppBundle:Evento')->findByPais($pais); 
      if (count($eventosPais)) {
        $paisesEvento[] = $pais;
      }
    }
    $listas = $em->getRepository('AppBundle:Evento')->findAll(); 
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $listas = $em->getRepository('AppBundle:Evento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    return $this->render('AdminBundle:Reportes:reporteListasNovios.html.twig', array(
      'listas'=> $listas,
      'paisR' => $request->get('pais'),
      'tipoR' => $request->get('tipo'),
      'tipoEventos' => $tipoEventos,
      'palabra' => $request->get('buscar'),
      'paises' => $paisesEvento,
      'desde' => $request->get('desde'),
      'hasta' => $request->get('hasta'),
    ));
  }

  /**
  * @Route("/newReporteListas" , name="newReporteListas")
  */
  public function newReporteListasAction(Request $request)
  {
  	$em =$this->getDoctrine()->getManager();
    $listas = $em->getRepository('AppBundle:Evento')->findAll(); 
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $listas = $em->getRepository('AppBundle:Evento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    $fecha = new \Datetime();
    $html2pdf = new Html2Pdf('L');
    $html2pdf->writeHTML($this->renderView('AdminBundle:Pdf:reporteListasNovios.html.twig',[
      'listas'=> $listas,
      'fecha'=> $fecha,
    ]));
    $html2pdf->output('reporteListasNovios.pdf','D');
  }

  /**
  * @Route("/reportePagos" , name="reportePagos")
  */
  public function pagosAction(Request $request)
  {
  	$em =$this->getDoctrine()->getManager();
  	$regalos = $em->getRepository('AppBundle:RegalosEvento')->findAll(); 
    $tipoEventos = $em->getRepository('AppBundle:tipoEvento')->findAll();
    $paises = $em->getRepository('AppBundle:Pais')->findAll();
    $paisesEvento = [];
    foreach ($paises as $pais) {
      $eventosPais = $em->getRepository('AppBundle:Evento')->findByPais($pais); 
      if (count($eventosPais)) {
        $paisesEvento[] = $pais;
      }
    }
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $regalos = $em->getRepository('AppBundle:RegalosEvento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    return $this->render('AdminBundle:Reportes:reportePagos.html.twig', array(
      'regalos'=> $regalos,
      'paisR' => $request->get('pais'),
      'tipoR' => $request->get('tipo'),
      'tipoEventos' => $tipoEventos,
      'palabra' => $request->get('buscar'),
      'paises' => $paisesEvento,
      'desde' => $request->get('desde'),
      'hasta' => $request->get('hasta'),
    ));
  }

  /**
  * @Route("/newReportePagos" , name="newReportePagos")
  */
  public function newReportePagosAction(Request $request)
  {
  	$em =$this->getDoctrine()->getManager();
  	$regalos = $em->getRepository('AppBundle:RegalosEvento')->findAll(); 
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $regalos = $em->getRepository('AppBundle:RegalosEvento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    $fecha = new \Datetime();
    $html2pdf = new Html2Pdf('L');
    $html2pdf->writeHTML($this->renderView('AdminBundle:Pdf:reportePagos.html.twig',[
      'regalos'=> $regalos,
      'fecha'=> $fecha,
    ]));
    $html2pdf->output('reportePagos.pdf','D');
  }

  /**
  * @Route("/reporteRegalos" , name="reporteRegalos")
  */
  public function regalosAction(Request $request )
  {
    $em =$this->getDoctrine()->getManager();
    $regalos = $em->getRepository('AppBundle:RegalosEvento')->findAll(); 
    $tipoEventos = $em->getRepository('AppBundle:tipoEvento')->findAll();
    $paises = $em->getRepository('AppBundle:Pais')->findAll();
    $paisesEvento = [];
    foreach ($paises as $pais) {
      $eventosPais = $em->getRepository('AppBundle:Evento')->findByPais($pais); 
      if (count($eventosPais)) {
        $paisesEvento[] = $pais;
      }
    }
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $regalos = $em->getRepository('AppBundle:RegalosEvento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    return $this->render('AdminBundle:Reportes:reporteRegalos.html.twig', array(
      'regalos'=> $regalos,
      'paisR' => $request->get('pais'),
      'tipoR' => $request->get('tipo'),
      'tipoEventos' => $tipoEventos,
      'palabra' => $request->get('buscar'),
      'paises' => $paisesEvento,
      'desde' => $request->get('desde'),
      'hasta' => $request->get('hasta'),
    ));
  }

  /**
  * @Route("/newReporteRegalos" , name="newReporteRegalos")
  */
  public function newReporteRegalosAction()
  {
    $em =$this->getDoctrine()->getManager();
    $regalos = $em->getRepository('AppBundle:RegalosEvento')->findAll(); 
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $regalos = $em->getRepository('AppBundle:RegalosEvento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    $fecha = new \Datetime();
    $html2pdf = new Html2Pdf('L');
    $html2pdf->writeHTML($this->renderView('AdminBundle:Pdf:reporteRegalos.html.twig',[
      'regalos'=> $regalos,
      'fecha'=> $fecha,
    ]));
    $html2pdf->output('reporteRegalos.pdf','D');
  }

  /**
  * @Route("/reporteCompleto" , name="reporteCompleto")
  */
  public function completoAction(Request $request)
  {
    $em =$this->getDoctrine()->getManager();
    $tipoEventos = $em->getRepository('AppBundle:tipoEvento')->findAll();  
    $eventos = $em->getRepository('AppBundle:Evento')->findAll();
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $eventos = $em->getRepository('AppBundle:Evento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais'), $request->get('desde') ,$request->get('hasta')); 
    }
    $listas = []; 
    foreach ($eventos as $evento) {
      $regalos = $em->getRepository('AppBundle:RegalosEvento')->findByEvento($evento);
      $listas[] = ['evento'=>$evento,'regalos'=>$regalos];
    }
    $paises = $em->getRepository('AppBundle:Pais')->findAll();
    $paisesEvento = [];
    foreach ($paises as $pais) {
      $eventosPais = $em->getRepository('AppBundle:Evento')->findByPais($pais); 
      if (count($eventosPais)) {
        $paisesEvento[] = $pais;
      }
    }
    return $this->render('AdminBundle:Reportes:reporteCompleto.html.twig', array(
      'listas'=> $listas,
      'eventos' => $eventos,
      'palabra' => $request->get('buscar'),
      'tipoEventos' => $tipoEventos,
      'paises' => $paisesEvento,
      'paisR' => $request->get('pais'),
      'tipoR' => $request->get('tipo'),
      'desde' => $request->get('desde'),
      'hasta' => $request->get('hasta'),
    ));
  }

  /**
  * @Route("/newReporteCompleto" , name="newReporteCompleto")
  */
  public function newReporteCompletoAction(Request $request)
  {
    $em =$this->getDoctrine()->getManager();
    $eventos = $em->getRepository('AppBundle:Evento')->findAll();
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $eventos = $em->getRepository('AppBundle:Evento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    $listas = []; 
    foreach ($eventos as $evento) {
      $regalos = $em->getRepository('AppBundle:RegalosEvento')->findByEvento($evento);
      $listas[] = ['evento'=>$evento,'regalos'=>$regalos];
    }
    $fecha = new \Datetime();
    $html2pdf = new Html2Pdf('L');
    $html2pdf->writeHTML($this->renderView('AdminBundle:Pdf:reporteCompleto.html.twig',[
      'listas'=> $listas,
      'fecha'=> $fecha,
    ]));
    $html2pdf->output('reporteCompleto.pdf','D');
  }

  /**
  * @Route("/newReporteListasExcel" , name="newReporteListasExcel")
  */
  public function newReporteListasExcelAction(Request $request){
    $em =$this->getDoctrine()->getManager();
    $listas = $em->getRepository('AppBundle:Evento')->findAll(); 
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $listas = $em->getRepository('AppBundle:Evento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    $fecha = new \Datetime();

     // ask the service for a Excel5
    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

    $phpExcelObject->getProperties()->setCreator("liuggio")
    ->setLastModifiedBy("Giulio De Donato")
    ->setTitle("Office 2005 XLSX Test Document")
    ->setSubject("Office 2005 XLSX Test Document")
    ->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
    ->setKeywords("office 2005 openxml php")
    ->setCategory("Test result file");
    $phpExcelObject->setActiveSheetIndex(0)
    ->setCellValue('A1', 'id')
    ->setCellValue('B1', 'Nombre de Lista')
    ->setCellValue('C1', 'Tipo de lista')
    ->setCellValue('D1', 'Fecha del evento')
    ->setCellValue('E1', 'Cantidad de invitados')
    ->setCellValue('F1', 'Pais')
    ->setCellValue('G1', 'Ciudad')
    ->setCellValue('H1', 'Pais retiro de fondos')
    ->setCellValue('I1', 'Moneda elegida');

    $row = 2; 
    for ($i=0; $i < count($listas) ; $i++) { 
      $phpExcelObject->setActiveSheetIndex(0)
      ->setCellValue('A'.$row, $listas[$i]->getId())
      ->setCellValue('B'.$row, $listas[$i]->getTitulo())
      ->setCellValue('C'.$row, $listas[$i]->getTipo()->getTipo())
      ->setCellValue('D'.$row, $listas[$i]->getFecha())
      ->setCellValue('E'.$row, $listas[$i]->getInvitados())
      ->setCellValue('F'.$row, $listas[$i]->getPais()->getPais())
      ->setCellValue('G'.$row, $listas[$i]->getCiudad())
      ->setCellValue('H'.$row, $listas[$i]->getComision()->getPais()->getPais())
      ->setCellValue('I'.$row, $listas[$i]->getComision()->getPais()->getCodigo());
      $row++;
    }



    $phpExcelObject->getActiveSheet()->setTitle('Simple');
       // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
    $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'CSV');
        // create the response
    $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
    $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      'ReporteListas '.date("d-m-Y").'.csv'
    );
    $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
    $response->headers->set('Pragma', 'public');
    $response->headers->set('Cache-Control', 'maxage=1');
    $response->headers->set('Content-Disposition', $dispositionHeader);

    return $response; 
  }

   /**
  * @Route("/newReportePagosExcel" , name="newReportePagosExcel")
  */
   public function newReportePagosExcelAction(Request $request){
    $em =$this->getDoctrine()->getManager();
    $regalos = $em->getRepository('AppBundle:RegalosEvento')->findAll(); 
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $regalos = $em->getRepository('AppBundle:RegalosEvento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    $fecha = new \Datetime();

     // ask the service for a Excel5
    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

    $phpExcelObject->getProperties()->setCreator("liuggio")
    ->setLastModifiedBy("Giulio De Donato")
    ->setTitle("Office 2005 XLSX Test Document")
    ->setSubject("Office 2005 XLSX Test Document")
    ->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
    ->setKeywords("office 2005 openxml php")
    ->setCategory("Test result file");
    $phpExcelObject->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Id ')
    ->setCellValue('B1', 'Id lista')
    ->setCellValue('C1', 'Medio de pago')
    ->setCellValue('D1', 'Fecha compra')
    ->setCellValue('E1', 'Monto')
    ->setCellValue('F1', 'Id Regalo')
    ->setCellValue('G1', 'Nombre lista')
    ->setCellValue('J1', 'Nombre regalo')
    ->setCellValue('I1', 'Mail titular')
    ->setCellValue('J1', 'Mail adicional')
    ->setCellValue('K1', 'Participantes de regalo')
    ->setCellValue('L1', 'Email participantes')
    ->setCellValue('M1', 'Dedicatoria')
    ->setCellValue('N1', 'Envie saludos (si/no)')
    ->setCellValue('O1', 'Agradecimiento de los novios');

    $row = 2; 
    for ($i=0; $i < count($regalos) ; $i++) { 
      $phpExcelObject->setActiveSheetIndex(0)
      ->setCellValue('A'.$row, $regalos[$i]->getId())
      ->setCellValue('B'.$row, $regalos[$i]->getEvento()->getId())
      ->setCellValue('C'.$row, $regalos[$i]->getFactura()->getMetodoPago())
      ->setCellValue('D'.$row, $regalos[$i]->getFactura()->getFecha())
      ->setCellValue('E'.$row, $regalos[$i]->getFactura()->getTotal())
      ->setCellValue('F'.$row, $regalos[$i]->getRegalo()->getId())
      ->setCellValue('G'.$row, $regalos[$i]->getEvento()->getTitulo())
      ->setCellValue('H'.$row, $regalos[$i]->getRegalo()->getNombre())
      ->setCellValue('I'.$row, $regalos[$i]->getEvento()->getEmail())
      ->setCellValue('J'.$row, $regalos[$i]->getEvento()->getEmail2())
      ->setCellValue('K'.$row, $regalos[$i]->getNombre())
      ->setCellValue('L'.$row, $regalos[$i]->getEmail())
      ->setCellValue('M'.$row, $regalos[$i]->getMensaje())
      ->setCellValue('N'.$row, $regalos[$i]->getAgradecer())
      ->setCellValue('O'.$row, $regalos[$i]->getAgradecerMensaje());
      $row++;
    }



    $phpExcelObject->getActiveSheet()->setTitle('Simple');
       // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
    $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'CSV');
        // create the response
    $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
    $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      'ReportePagos '.date("d-m-Y").'.csv'
    );
    $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
    $response->headers->set('Pragma', 'public');
    $response->headers->set('Cache-Control', 'maxage=1');
    $response->headers->set('Content-Disposition', $dispositionHeader);

    return $response; 
  }


   /**
  * @Route("/newReporteRegalosExcel" , name="newReporteRegalosExcel")
  */
   public function newReporteRegalosExcelAction(Request $request){
    $em =$this->getDoctrine()->getManager();
    $regalos = $em->getRepository('AppBundle:RegalosEvento')->findAll(); 
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $regalos = $em->getRepository('AppBundle:RegalosEvento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    $fecha = new \Datetime();

     // ask the service for a Excel5
    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

    $phpExcelObject->getProperties()->setCreator("liuggio")
    ->setLastModifiedBy("Giulio De Donato")
    ->setTitle("Office 2005 XLSX Test Document")
    ->setSubject("Office 2005 XLSX Test Document")
    ->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
    ->setKeywords("office 2005 openxml php")
    ->setCategory("Test result file");
    $phpExcelObject->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Id ')
    ->setCellValue('B1', 'Id lista')
    ->setCellValue('C1', 'Tipo de Evento')
    ->setCellValue('D1', 'Id Regalo')
    ->setCellValue('E1', 'Fecha compra')
    ->setCellValue('F1', 'Monto')
    ->setCellValue('G1', 'Nombre lista')
    ->setCellValue('H1', 'Nombre regalo')
    ->setCellValue('I1', 'Participantes de regalo')
    ->setCellValue('J1', 'Email participantes')
    ->setCellValue('K1', 'Mail titular')
    ->setCellValue('L1', 'Mail adicional');

    $row = 2; 
    for ($i=0; $i < count($regalos) ; $i++) { 
      $phpExcelObject->setActiveSheetIndex(0)
      ->setCellValue('A'.$row, $regalos[$i]->getId())
      ->setCellValue('B'.$row, $regalos[$i]->getEvento()->getId())
      ->setCellValue('C'.$row, $regalos[$i]->getEvento()->getTipo()->getTipo())
      ->setCellValue('D'.$row, $regalos[$i]->getRegalo()->getId())
      ->setCellValue('E'.$row, $regalos[$i]->getFactura()->getFecha())
      ->setCellValue('F'.$row, $regalos[$i]->getFactura()->getTotal())
      ->setCellValue('G'.$row, $regalos[$i]->getEvento()->getTitulo())
      ->setCellValue('H'.$row, $regalos[$i]->getRegalo()->getNombre())
      ->setCellValue('I'.$row, $regalos[$i]->getNombre())
      ->setCellValue('J'.$row, $regalos[$i]->getEmail())
      ->setCellValue('K'.$row, $regalos[$i]->getEvento()->getEmail())
      ->setCellValue('L'.$row, $regalos[$i]->getEvento()->getEmail2());
      $row++;
    }



    $phpExcelObject->getActiveSheet()->setTitle('Simple');
       // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
    $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'CSV');
        // create the response
    $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
    $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      'ReporteRegalos '.date("d-m-Y").'.csv'
    );
    $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
    $response->headers->set('Pragma', 'public');
    $response->headers->set('Cache-Control', 'maxage=1');
    $response->headers->set('Content-Disposition', $dispositionHeader);

    return $response; 
  }

  /**
  * @Route("/newReporteCompletoExcel" , name="newReporteCompletoExcel")
  */
  public function newReporteCompletoExcelAction(Request $request){
    $em =$this->getDoctrine()->getManager();
    $eventos = $em->getRepository('AppBundle:Evento')->findAll();
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo') || $request->get('desde') || $request->get('hasta')) {
      $eventos = $em->getRepository('AppBundle:Evento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais') , $request->get('desde') ,$request->get('hasta')); 
    }
    $fecha = new \Datetime();

    $listas = []; 
    foreach ($eventos as $evento) {
      $regalos = $em->getRepository('AppBundle:RegalosEvento')->findByEvento($evento);
      $listas[] = ['evento'=>$evento,'regalos'=>$regalos];
    }

     // ask the service for a Excel5
    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

    $phpExcelObject->getProperties()->setCreator("liuggio")
    ->setLastModifiedBy("Giulio De Donato")
    ->setTitle("Office 2005 XLSX Test Document")
    ->setSubject("Office 2005 XLSX Test Document")
    ->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
    ->setKeywords("office 2005 openxml php")
    ->setCategory("Test result file");

    $row = 1; 
    for ($i=0; $i < count($listas) ; $i++) {
      $phpExcelObject->setActiveSheetIndex(0)
      ->setCellValue('A'.$row, 'id')
      ->setCellValue('B'.$row, 'Nombre de Lista')
      ->setCellValue('C'.$row, 'Tipo de lista')
      ->setCellValue('D'.$row, 'Fecha del evento')
      ->setCellValue('E'.$row, 'Cantidad de invitados')
      ->setCellValue('F'.$row, 'Pais')
      ->setCellValue('G'.$row, 'Ciudad')
      ->setCellValue('H'.$row, 'Pais retiro de fondos')
      ->setCellValue('I'.$row, 'Moneda elegida');
      $row++;
      $phpExcelObject->setActiveSheetIndex(0)
      ->setCellValue('A'.$row, $listas[$i]['evento']->getId())
      ->setCellValue('B'.$row, $listas[$i]['evento']->getTitulo())
      ->setCellValue('C'.$row, $listas[$i]['evento']->getTipo()->getTipo())
      ->setCellValue('D'.$row, $listas[$i]['evento']->getFecha())
      ->setCellValue('E'.$row, $listas[$i]['evento']->getInvitados())
      ->setCellValue('F'.$row, $listas[$i]['evento']->getPais()->getPais())
      ->setCellValue('G'.$row, $listas[$i]['evento']->getCiudad())
      ->setCellValue('H'.$row, $listas[$i]['evento']->getComision()->getPais()->getPais())
      ->setCellValue('I'.$row, $listas[$i]['evento']->getComision()->getPais()->getCodigo());
      $row++;
      $phpExcelObject->setActiveSheetIndex(0)
      ->setCellValue('A'.$row, 'Id ')
      ->setCellValue('B'.$row, 'Id lista')
      ->setCellValue('C'.$row, 'Tipo de Evento')
      ->setCellValue('D'.$row, 'Id Regalo')
      ->setCellValue('E'.$row, 'Fecha compra')
      ->setCellValue('F'.$row, 'Monto')
      ->setCellValue('G'.$row, 'Nombre lista')
      ->setCellValue('H'.$row, 'Nombre regalo')
      ->setCellValue('I'.$row, 'Participantes de regalo')
      ->setCellValue('J'.$row, 'Email participantes')
      ->setCellValue('K'.$row, 'Mail titular')
      ->setCellValue('L'.$row, 'Mail adicional');
      $row++;
      for ($j=0; $j < count($listas[$i]['regalos']); $j++) { 
        $phpExcelObject->setActiveSheetIndex(0)
        ->setCellValue('A'.$row, $listas[$i]['regalos'][$j]->getId())
        ->setCellValue('B'.$row, $listas[$i]['regalos'][$j]->getEvento()->getId())
        ->setCellValue('C'.$row, $listas[$i]['regalos'][$j]->getEvento()->getTipo()->getTipo())
        ->setCellValue('D'.$row, $listas[$i]['regalos'][$j]->getRegalo()->getId())
        ->setCellValue('E'.$row, $listas[$i]['regalos'][$j]->getFactura()->getFecha())
        ->setCellValue('F'.$row, $listas[$i]['regalos'][$j]->getFactura()->getTotal())
        ->setCellValue('G'.$row, $listas[$i]['regalos'][$j]->getEvento()->getTitulo())
        ->setCellValue('H'.$row, $listas[$i]['regalos'][$j]->getRegalo()->getNombre())
        ->setCellValue('I'.$row, $listas[$i]['regalos'][$j]->getNombre())
        ->setCellValue('J'.$row, $listas[$i]['regalos'][$j]->getEmail())
        ->setCellValue('K'.$row, $listas[$i]['regalos'][$j]->getEvento()->getEmail())
        ->setCellValue('L'.$row, $listas[$i]['regalos'][$j]->getEvento()->getEmail2());
        $row++;
      }
      $row++;
    }



    $phpExcelObject->getActiveSheet()->setTitle('Simple');
       // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
    $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'CSV');
        // create the response
    $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
    $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      'ReporteCompleto '.date("d-m-Y").'.csv'
    );
    $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
    $response->headers->set('Pragma', 'public');
    $response->headers->set('Cache-Control', 'maxage=1');
    $response->headers->set('Content-Disposition', $dispositionHeader);

    return $response; 
  }



}
