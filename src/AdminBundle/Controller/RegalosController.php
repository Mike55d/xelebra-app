<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Regalo;

    /**
     * @Route("regalos")
     */
    class RegalosController extends Controller
    {
    /**
     * @Route("/" , name="AdminRegalos_index")
     */
    public function indexAction()
    {
        $em =$this->getDoctrine()->getManager(); 
        
        $regalos = $em->getRepository('AppBundle:Regalo')->findAll(); 
        return $this->render('AdminBundle:Regalos:index.html.twig', array(
            'regalos' => $regalos,
        ));
    }

    /**
     * @Route("/new" , name="AdminRegalos_new")
     */
    public function newAction(Request $request)
    {
        $em =$this->getDoctrine()->getManager(); 
        $categorias = $em->getRepository('AppBundle:Categoria')->findALl(); 
        if ($request->get('nombre')) {
            $regalo = new Regalo;
            $categoria = $em->getRepository('AppBundle:Categoria')
            ->find($request->get('categoria'));
            $regalo->setCategoria($categoria);
            $regalo->setNombre($request->get('nombre'));
            $file = $request->files->get('image');
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('images'),$fileName);
            $regalo->setImage($fileName);
            $regalo->setLista($request->get('lista'));
            $regalo->setPrecio($request->get('precio'));
            $regalo->setSugerido($request->get('sugerido'));
            $regalo->setDescripcion($request->get('descripcion'));
            $em->persist($regalo);
            $em->flush();
            return $this->redirectToRoute('AdminRegalos_index');
        }
        return $this->render('AdminBundle:Regalos:new.html.twig', array(
            'categorias' => $categorias
        ));
    }

    /**
     * @Route("/{id}/edit" , name="AdminRegalos_edit")
     */
    public function editAction(Regalo $regalo ,Request $request)
    {
        $em =$this->getDoctrine()->getManager(); 
        $categorias = $em->getRepository('AppBundle:Categoria')->findALl(); 
        if ($request->get('nombre')) {
            $categoria = $em->getRepository('AppBundle:Categoria')
            ->find($request->get('categoria'));
            $regalo->setCategoria($categoria);
            $regalo->setNombre($request->get('nombre'));
            if ($request->files->get('image')) {
                $file = $request->files->get('image');
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move($this->getParameter('images'),$fileName);
                $regalo->setImage($fileName);
            }
            $regalo->setLista($request->get('lista'));
            $regalo->setPrecio($request->get('precio'));
            $regalo->setSugerido($request->get('sugerido'));
            $regalo->setDescripcion($request->get('descripcion'));
            $em->flush();
            return $this->redirectToRoute('AdminRegalos_index');
        }
        return $this->render('AdminBundle:Regalos:edit.html.twig', array(
            'categorias' => $categorias,
            'regalo' => $regalo,
        ));
    }

    /**
     * @Route("/{id}/del" , name="AdminRegalos_del")
     */
    public function delAction(Regalo $regalo)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($regalo);
        $em->flush();
        return $this->redirectToRoute('AdminRegalos_index');
    }

}
