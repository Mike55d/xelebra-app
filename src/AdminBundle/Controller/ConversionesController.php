<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Conversion;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\CurlHttpClient;

/**
     * @Route("Conversiones" )
     */
class ConversionesController extends Controller
{
    /**
     * @Route("/" , name="AdminConversiones_index" )
     */
    public function indexAction()
    {
        $em =$this->getDoctrine()->getManager(); 
        $conversiones = $em->getRepository('AppBundle:Conversion')->findAll(); 
        return $this->render('AdminBundle:Conversiones:index.html.twig', array(
            'conversiones'=> $conversiones
        ));
    }

    /**
     * @Route("/actualizar" , name ="AdminConversiones_actualizar")
     */
    public function actualizarAction()
    {
        $em =$this->getDoctrine()->getManager(); 
        $client = new CurlHttpClient();
        $client = HttpClient::create([
        'headers'=>['Authorization' => 'Bearer Zp_GChQHxk6e5IZnCL-dl0EXKbKKZ2tDTpbvPDzTOiEI5-eJFslx3']
        ]);
        $response = $client->request('GET', 'https://currency.labstack.com/api/v1/rates');
        $content = $response->toArray();
        $monedas = $em->getRepository('AppBundle:Conversion')->findAll(); 
        foreach ($content['rates'] as $monedaApi => $valorApi) {
            foreach ($monedas as $moneda) {
                if ($moneda->getMoneda() == $monedaApi) {
                    $moneda->setConversion($valorApi);
                    $moneda->setFecha(new \Datetime());
                    $em->flush();
                }
            }
        }
        // return $content;
        return $this->redirectToRoute('AdminConversiones_index');
    }

    /**
     * @Route("/{id}/edit" , name ="AdminConversiones_edit")
     */
    public function editAction(Conversion $conversion , Request $request)
    {
        $em =$this->getDoctrine()->getManager();
        if ($request->get('conv')) {
            $conversion->setConversion($request->get('conv'));
            $em->flush();
            return $this->redirectToRoute('AdminConversiones_index');
        }
        return $this->render('AdminBundle:Conversiones:edit.html.twig', array(
            'conversion' => $conversion
        ));
    }

}
