<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\textoEditable;
use Symfony\Component\HttpFoundation\Request;

/**
  * @Route("editables")
  */
class EditablesController extends Controller
{
  /**
  * @Route("/" , name="editables_index")
  */
  public function indexAction()
  {
  	$em =$this->getDoctrine()->getManager();
  	$editables = $em->getRepository('AppBundle:textoEditable')->findAll();
    return $this->render('AdminBundle:Editables:index.html.twig',[
    	'editables' => $editables,
    ]);
  }

  /**
  * @Route("/{id}/edit" , name="editables_edit")
  */
  public function editAction(textoEditable $editable , Request $request)
  {
  	$em =$this->getDoctrine()->getManager();
    if ($request->get('texto')) {
      $editable->setTexto($request->get('texto'));
      $em->flush();
      return $this->redirectToRoute('editables_index');
    }
    return $this->render('AdminBundle:Editables:edit.html.twig',[
    	'editable' => $editable,
    ]);
  }
}
