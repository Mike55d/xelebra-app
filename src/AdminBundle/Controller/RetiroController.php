<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Regalo;
use AppBundle\Entity\Categoria;

/**
     * @Route("/retiro")
     */
class RetiroController extends Controller
{
    /**
     * @Route("/{tipo}/retiros" , name="adminRetiro_index")
     */
    public function pendientesAction($tipo , Request $request )
    {
        $em =$this->getDoctrine()->getManager(); 
        if ($tipo == 'todos') {
            $retiros = $em->getRepository('AppBundle:Retiro')->findAll(); 
        }else{
            $retiros = $em->getRepository('AppBundle:Retiro')->findByStatus($tipo); 
        }
        return $this->render('AdminBundle:Retiro:retiros.html.twig', array(
            'retiros'=>$retiros,
            'tipo'=>$tipo
        ));
    }

    /**
     * @Route("/{tipo}/{id}/pagarRetiro" , name="adminPagarRetiro")
     */
    public function pagarRetirosAction($tipo , $id)
    {
        $em =$this->getDoctrine()->getManager(); 
        $retiro = $em->getRepository('AppBundle:Retiro')->find($id); 
        $retiro->setStatus('pagado');
        $em->flush();
        $message = (new \Swift_Message('Notificacion'))
            ->setSubject('Notificaciones')
            ->setFrom('info@financeirocheznous.org')
            ->setTo($retiro->getUser()->getUsername())
            ->setBody(
                $this->renderView(
                    'AppBundle:Email:retiro.html.twig',
                    array('retiro' => $retiro)),'text/html');
        $this->get('mailer')->send($message);
        return $this->redirectToRoute('adminRetiro_index',['tipo'=>$tipo]);
    }


    /**
     * @Route("/subirExcel" , name="admin_subirExcel")
     */
    public function subirExcel(Request $request)
    {
        $data = [];
        if (!empty($request->files->get('archivo'))){
        $archivo = $request->files->get('archivo')->getRealPath();
        $appPath = $this->container->getParameter('kernel.root_dir');
        $file = realpath($appPath . '/../web/excelFiles/productos.xlsx');

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($archivo);
        $sheet = $phpExcelObject->getActiveSheet()->toArray(null, true, true, true);

        $em = $this->getDoctrine()->getManager();
        $data['sheet'] = $sheet;
        //READ EXCEL FILE CONTENT
        foreach($sheet as $i=>$row) {
            
        if($i !== 1) {
            $regalo = new Regalo;
            $regalo->setNombre($row['B']);
            $regalo->setDescripcion($row['C']);
            $regalo->setPrecio(str_replace(',', '', $row['D']));
            $regalo->setSugerido(str_replace(',', '', $row['E']));
            $regalo->setImage($row['F']);
            $categoria = $em->getRepository('AppBundle:Categoria')
            ->findOneByCategoria($row['G']);
            if (!$categoria) {
                $categoria = new Categoria;
                $categoria->setCategoria($row['G']);
                $em->persist($categoria);
                $em->flush();
             } 
            $regalo->setCategoria($categoria);
            $tipoLista = explode('-', $row['H']);
            $regalo->setLista($tipoLista[1]);
            if ($tipoLista[0] == 'regalos casamiento ') {
                $tipoEvento = $em->getRepository('AppBundle:tipoEvento')->find(1); 
                $regalo->setTipo($tipoEvento);
            }
            if ($tipoLista[0] == 'luna de miel ') {
                $tipoEvento = $em->getRepository('AppBundle:tipoEvento')->find(3); 
                $regalo->setTipo($tipoEvento);
            }
            if ($tipoLista[0] == 'baby shower ') {
                $tipoEvento = $em->getRepository('AppBundle:tipoEvento')->find(2); 
                $regalo->setTipo($tipoEvento);
            }
            $em->persist($regalo);
            $em->flush();
            //redirect appropriately
            }
        }
        $data['obj'] = $phpExcelObject;
        }
    }

}
