<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Categoria;

/**
     * @Route("/categorias")
     */
class CategoriasController extends Controller
{
    /**
     * @Route("/" , name="AdminCategorias_index")
     */
    public function indexAction()
    {
        $em =$this->getDoctrine()->getManager(); 
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll(); 
        return $this->render('AdminBundle:Categorias:index.html.twig', array(
            'categorias'=> $categorias
        ));
    }

    /**
     * @Route("/new" , name="AdminCategorias_new")
     */
    public function newAction(Request $request ) 
    {
        $em =$this->getDoctrine()->getManager(); 
        if ($request->get('categoria')) {
            $categoria = new Categoria;
            $categoria->setCategoria($request->get('categoria'));
            $em->persist($categoria);
            $em->flush();
            return $this->redirectToRoute('AdminCategorias_index');
        }
        return $this->render('AdminBundle:Categorias:new.html.twig', array(
            
        ));
    }

    /**
     * @Route("/{id}/edit" , name="AdminCategorias_edit")
     */
    public function editAction(Categoria $categoria , Request $request )
    {
        $em =$this->getDoctrine()->getManager(); 
        if ($request->get('Categoria')) {
            $categoria->setCategoria($request->get('Categoria'));
            $em->flush();
            return $this->redirectToRoute('AdminCategorias_index');
        }
        return $this->render('AdminBundle:Categorias:edit.html.twig', array(
            'categoria'=>$categoria
        ));
    }

    /**
     * @Route("/{id}/del" , name="AdminCategorias_del")
     */
    public function delAction(Categoria $categoria)
    {
        $em =$this->getDoctrine()->getManager(); 
        $em->remove($categoria);
        $em->flush();
        return $this->redirectToRoute('AdminCategorias_index');
    }

}
