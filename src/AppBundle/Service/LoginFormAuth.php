<?php 
namespace AppBundle\Service;

use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoginFormAuth extends AbstractFormLoginAuthenticator
{
    private $formFactory;
    private $em;
    private $router;
    private $encoder;
    public function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $em, RouterInterface $router ,UserPasswordEncoderInterface $pwEncoder)
    {
        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->router = $router;
        $this->encoder = $pwEncoder;
    }

    public function getCredentials(Request $request )
    {
        $isLoginSubmit = $request->getPathInfo() == '/register' && $request->isMethod('POST');
        if (!$isLoginSubmit) {
           // skip authentication
           return;
       }
       $username = $request->get('email');
       $user = $this->em->getRepository('AppBundle:User')
       ->findOneBy(['username' => $username]);
       if ($user) {
           return null;
       }else{
        $user = new User;
        $user->setUsername($request->get('email'));
        $user->setRoles("ROLE_USER");
        $user->setRola("USER");
        $user->setActive(1);
        $password = $this->encoder
        ->encodePassword($user,$request->get('pw'));
        $user->setPassword($password);
        $user->setSaldo(0);
        $em->persist($user);
        $em->flush();
       }
       return $request;

   }
   public function checkCredentials($credentials, UserInterface $user){
    return true;
}

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 403 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     *
     * @return Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception){
 // change "app_homepage" to some route in your app
        $targetUrl = $this->router->generate('users_register',['error' => 'Este email ya esta registrado']);

        return new RedirectResponse($targetUrl);
    }

    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If you return null, the current request will continue, and the user
     * will be authenticated. This makes sense, for example, with an API.
     *
     * @param string $providerKey The provider (i.e. firewall) key
     *
     * @return Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey){
         // change "app_homepage" to some route in your app
        $targetUrl = $this->router->generate('homepage');

        return new RedirectResponse($targetUrl);
    }

    /**
     * Does this method support remember me cookies?
     *
     * Remember me cookie will be set if *all* of the following are met:
     *  A) This method returns true
     *  B) The remember_me key under your firewall is configured
     *  C) The "remember me" functionality is activated. This is usually
     *      done by having a _remember_me checkbox in your form, but
     *      can be configured by the "always_remember_me" and "remember_me_parameter"
     *      parameters under the "remember_me" firewall key
     *  D) The onAuthenticationSuccess method returns a Response object
     *
     * @return bool
     */
    public function supportsRememberMe(){

    }

    public function getLoginUrl(){
        //nos determina a donde ir si el login falla
        return $this->router->generate('blue');
    }

    public function getUser($credentials, UserProviderInterface  $userProvider){
        $username = $credentials->get('email');
        return $this->em->getRepository('AppBundle:User')
        ->findOneBy(['username' => $username]);
    }

    // ...
}