<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Retiro;
use AppBundle\Entity\Evento;

/**
     * @Route("{_locale}/retiro")
     */
class RetiroController extends Controller
{
    /**
     * @Route("/" , name="retiro_index")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Retiro:index.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/{evento}/new" , name="retiro_new")
     */
    public function newPaypalAction(Evento $evento,Request $request , \Swift_Mailer $mailer)
    {
        $em =$this->getDoctrine()->getManager(); 
        $user = $this->get('security.token_storage')
        ->getToken()->getUser();
        if ($evento->getTipoCobro() == 'fijo') {
            $comision = $evento->getComision()->getFijo();
        }else{
            $comision = $evento->getComision()->getPorcentNovio();
        }
        $disclaimer = $em->getRepository('AppBundle:textoEditable')->findOneBySeccion('disclaimer'); 
        if ($request->get('monto')) {
            $retiro = new Retiro;
            $retiro->setMonto($request->get('recibir'));
            $retiro->setTitular($request->get('titular'));
            $retiro->setFecha(new \Datetime());
            $retiro->setUser($user);
            $retiro->setDni($request->get('dni'));
            $retiro->setCuit($request->get('cuit'));
            $retiro->setNombreBanco($request->get('nombreBanco'));
            $retiro->setNumeroCuenta($request->get('numeroCuenta'));
            $retiro->setCbu($request->get('cbu'));
            $retiro->setTipoCuenta($request->get('tipoCuenta'));
            $retiro->setCiudadSucursal($request->get('ciudadSucursal'));
            $retiro->setCodigoPostal($request->get('codigoPostal'));
            $retiro->setMetodo('Transferencia');
            $retiro->setStatus('procesando');
            $evento->setSaldo($evento->getSaldo() - intval($request->get('monto')));
            $em->persist($retiro);
            $em->flush();
            $message = (new \Swift_Message('Notificacion'))
            ->setSubject('Recibimos tu solicitud de retiro de fondos')
            ->setFrom('info@financeirocheznous.org')
            ->setTo($evento->getEmail())
            ->setBody(
                $this->renderView(
                    'AppBundle:Email:newRetiro.html.twig',
                    array('retiro' => $retiro , 'evento'=> $evento)),'text/html');
            $mailer->send($message);
            return $this->redirectToRoute('retiro_enviado');
        }
        return $this->render('AppBundle:Retiro:newPaypal.html.twig', array(
            'comision' => $comision,
            'evento'=> $evento,
            'tipoCobro' => $evento->getTipoCobro(),
            'disclaimer' => $disclaimer,
        ));
    }

     /**
     * @Route("/{evento}/newUSA" , name="retiro_newUSA")
     */
    public function newUSAAction(Evento $evento,Request $request , \Swift_Mailer $mailer)
    {
        $em =$this->getDoctrine()->getManager(); 
        $user = $this->get('security.token_storage')
        ->getToken()->getUser();
        if ($evento->getTipoCobro() == 'fijo') {
            $comision = $evento->getComision()->getFijo();
        }else{
            $comision = $evento->getComision()->getPorcentNovio();
        }
        $disclaimer = $em->getRepository('AppBundle:textoEditable')->findOneBySeccion('disclaimer'); 
        if ($request->get('monto')) {
            $retiro = new Retiro;
            $retiro->setMonto($request->get('recibir'));
            $retiro->setTitular($request->get('titular'));
            $retiro->setFecha(new \Datetime());
            $retiro->setUser($user);

            $retiro->setRecipientName($request->get('recipientName'));
            $retiro->setBankName($request->get('bankName'));
            $retiro->setBankCity($request->get('bankCity'));
            $retiro->setAbaRouting($request->get('abaRouting'));
            $retiro->setAccountNumber($request->get('accountNumber'));
            $retiro->setTipoCuenta($request->get('tipoCuenta'));
            $retiro->setintermediaryBank($request->get('intermediaryBank'));
            $retiro->setBeneficiaryName($request->get('beneficiaryName'));
            $retiro->setCodigoSwift($request->get('codigoSwift'));
            $retiro->setBeneficiaryAccount($request->get('beneficiaryAccount'));
            $retiro->setIntermediaryBankSwift($request->get('intermediaryBankSwift'));
            $retiro->setStreetAddress($request->get('streetAddress'));
            $retiro->setCity($request->get('city'));
            $retiro->setCountry($request->get('country'));
            $retiro->setCodigoPostal($request->get('codigoPostal'));

            $retiro->setMetodo('Transferencia');
            $retiro->setStatus('procesando');
            $evento->setSaldo($evento->getSaldo() - intval($request->get('monto')));
            $em->persist($retiro);
            $em->flush();
            $message = (new \Swift_Message('Notificacion'))
            ->setSubject('Recibimos tu solicitud de retiro de fondos')
            ->setFrom('info@financeirocheznous.org')
            ->setTo($evento->getEmail())
            ->setBody(
                $this->renderView(
                    'AppBundle:Email:newRetiro.html.twig',
                    array('retiro' => $retiro , 'evento'=> $evento)),'text/html');
            $mailer->send($message);
            return $this->redirectToRoute('retiro_enviado');
        }
        return $this->render('AppBundle:Retiro:newUSA.html.twig', array(
            'comision' => $comision,
            'evento'=> $evento,
            'tipoCobro' => $evento->getTipoCobro(),
            'disclaimer' => $disclaimer,
        ));
    }


    /**
     * @Route("/newMercadopago" , name="retiro_new_mp")
     */
    public function newMpAction(Request $request)
    {
        $em =$this->getDoctrine()->getManager(); 
        $user = $this->get('security.token_storage')
        ->getToken()->getUser(); 
        if ($request->get('monto')) {
            $retiro = new Retiro;
            $retiro->setMonto($request->get('monto'));
            $retiro->setFecha(new \Datetime());
            $retiro->setUser($user);
            $retiro->setMetodo('mercadoPago');
            $retiro->setStatus('procesando');
            $evento->setSaldo($evento->getSaldo() - intval($request->get('monto')));
            $em->persist($retiro);
            $em->flush();
            return $this->redirectToRoute('retiro_enviado');
        }
        return $this->render('AppBundle:Retiro:newMp.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/retiroEnviado" , name="retiro_enviado")
     */
    public function retiroEnviadoAction()
    {
        return $this->render('AppBundle:Retiro:retiroEnviado.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/{id}/cancelarRetiro" , name="retiro_cancelar")
     */
    public function cancelarAction(Retiro $retiro , Request $request)
    {
        $user = $this->get('security.token_storage')
        ->getToken()->getUser(); 
        $em =$this->getDoctrine()->getManager(); 
        if ($request->get('cancelar')) {
            if ($retiro->getUser()->getId() == $user->getId() && $retiro->getStatus() == 'procesando') {
                $retiro->setStatus('cancelado');
                $evento = $em->getRepository('AppBundle:Evento')
                ->find($retiro->getEvento()->getId()); 
                $evento->setSaldo($evento->getSaldo() + $retiro->getMonto());
                $em->flush();
            }
            return $this->redirectToRoute('retiro_cancelado');          
        }
        return $this->render('AppBundle:Retiro:retiroCancelar.html.twig', array(
            'retiro' => $retiro
        ));
    }

    /**
     * @Route("/retiroCancelado" , name="retiro_cancelado")
     */
    public function canceladoAction()
    {
        return $this->render('AppBundle:Retiro:retiroCancelado.html.twig', array(
        ));
    }

    /**
     * @Route("/misRetiros" , name="mis_retiros")
     */
    public function misRetirosAction()
    {
        $em =$this->getDoctrine()->getManager(); 
        $user = $this->get('security.token_storage')
        ->getToken()->getUser(); 
        $misRetiros = $em->getRepository('AppBundle:Retiro')->findByUser($user); 
        return $this->render('AppBundle:Retiro:misRetiros.html.twig', array(
            'misRetiros' => $misRetiros
        ));
    }

}
