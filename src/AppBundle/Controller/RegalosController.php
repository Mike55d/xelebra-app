<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Evento;


/**
     * @Route("{_locale}/regalos")
     */
class RegalosController extends Controller
{
    /**
     * @Route("/{id}/misEventos" , name="regalos_index")
     */
    public function misRegalosAction(Evento $evento ,Request $request , \Swift_Mailer $mailer)
    {
        $em =$this->getDoctrine()->getManager(); 
        $user = $this->get('security.token_storage')
        ->getToken()->getUser(); 
        $facturas = $em->getRepository('AppBundle:Factura')->findByEvento($evento->getId());
        $retiros = $em->getRepository('AppBundle:Retiro')->findBy(['user'=>$user->getId(),'status'=>'procesando']); 
        $retirosPagados = $em->getRepository('AppBundle:Retiro')->findBy(['user'=>$user->getId(),'status'=>'pagado']); 
        $total = 0;
        $retiroPendiente = 0;
        $retiroPagado = 0;
        $eventos = $em->getRepository('AppBundle:Evento')->findByUser($user); 
        $totalCuenta = 0;
        foreach ($facturas as $factura) {
            $total+= $factura->getTotal();
        }
        foreach ($eventos as $event) {
            $totalCuenta+= $event->getSaldo();
        }
        foreach ($retiros as $retiro) {
            $retiroPendiente+= $retiro->getMonto();
        }
        foreach ($retirosPagados as $retiro) {
            $retiroPagado+= $retiro->getMonto();
        }
        $regalos = $em->getRepository('AppBundle:RegalosEvento')->findByEvento($evento->getId()); 
        if ($request->get('mensaje')) {
            $regalo = $em->getRepository('AppBundle:RegalosEvento')->find($request->get('idregalo')); 
            $regalo->setAgradecer('si');
            $regalo->setAgradecerMensaje($request->get('mensaje'));
            $em->flush();
            $message = (new \Swift_Message('Notificacion'))
            ->setSubject('Notificaciones')
            ->setFrom('info@financeirocheznous.org')
            ->setTo($regalo->getEmail())
            ->setBody(
                $this->renderView(
                    'AppBundle:Email:agradecer.html.twig',
                    array('regalo' => $regalo)),'text/html');
            $mailer->send($message);
        }
        $fecha_actual = date("d-m-Y");
        $data[date("F", strtotime($fecha_actual))]= [];
        for ($i=0; $i < 3; $i++) { 
            $mesAnterior = date("d-m-Y",strtotime($fecha_actual."- 1 month"));
            $fecha_actual = $mesAnterior;
            $data[date("F", strtotime($mesAnterior))]= [];
        }
        $data = array_reverse($data);
        foreach ($regalos as $regalo) {
            $existsMonth = array_key_exists(date("F", strtotime($regalo->getFactura()->getFecha()->format('Y-m-d'))),$data);
            if ($existsMonth) {
                $existsDay = array_key_exists(date("d" , strtotime($regalo->getFactura()->getFecha()->format('Y-m-d'))),
                $data[date("F", strtotime($regalo->getFactura()->getFecha()->format('Y-m-d')))]);
                if ($existsDay) {
                    array_push(
                        $data
                        [date("F", strtotime($regalo->getFactura()->getFecha()->format('Y-m-d')))]
                        [date("d" , strtotime($regalo->getFactura()->getFecha()->format('Y-m-d')))]
                        ['gifts']
                        ,$regalo);
                }else{
                   $data
                   [date("F", strtotime($regalo->getFactura()->getFecha()->format('Y-m-d')))]
                   [date("d" , strtotime($regalo->getFactura()->getFecha()->format('Y-m-d')))]
                   ['gifts']
                    = [$regalo];
               }
           }
       }
       foreach ($data as $month => $day) {
           foreach ($day as $day => $gifts) {
            $totalRegalos = 0;
               foreach ($gifts['gifts'] as $regalo) {
                   $totalRegalos+=$regalo->getContribucion();
                   $data[$month][$day]['total'] = $totalRegalos;
               }
           }
       }

       return $this->render('AppBundle:Regalos:index.html.twig', array(
        'evento'=>$evento,
        'regalos'=>$regalos,
        'total'=> $total,
        'retiroPendiente' =>$retiroPendiente,
        'retiroPagado' => $retiroPagado,
        'totalCuenta' => $totalCuenta,
        'data' => $data,
        'eventos' => $eventos,
    ));
   }

    /**
     * @Route("/new" , name="regalos_new")
     */
    public function newAction()
    {
        return $this->render('AppBundle:Regalos:new.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/edit" , name="regalos_edit")
     */
    public function editAction()
    {
        return $this->render('AppBundle:Regalos:edit.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/del" , name="regalos_del")
     */
    public function delAction()
    {
        return $this->render('AppBundle:Regalos:del.html.twig', array(
            // ...
        ));
    }

}
