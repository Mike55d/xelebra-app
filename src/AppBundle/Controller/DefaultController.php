<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Service\MyService;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
  /**
   * @Route("{_locale}/dashboard", name="homepage")
   */
  public function indexAction(Request $request ,MyService $messageGenerator)
  {
    // thanks to the type-hint, the container will instantiate a
    // new MessageGenerator and pass it to you!
    // ...

    $message = $messageGenerator->getHappyMessage();
    $this->addFlash('success', $message);
    // ...

    $locale = $request->getLocale();
    $user = $this->get('security.token_storage')
    ->getToken()->getUser(); 
    if ($user != 'anon.' && $user->getRola() == 'ADMIN') {
      return $this->redirectToRoute('homepage_admin');
    }
    // $em =$this->getDoctrine()->getManager(); 
    // $comisiones = $em->getRepository('AppBundle:Comisiones')->findALl();
    // foreach ($comisiones as $comision) {
    //    $comision->setImpuestoPais(1.05);
    //    $em->flush();
    //  } 
    $translated = $this->get('translator')->trans('Symfony is grea');
    return $this->render('AppBundle:Home:index.html.twig',[
      'var' => $translated,
      'locale'=>$locale,
      'message'=>$message
    ]);
  }

  /**
   * @Route("/", name="home")
   */
  public function homeAction(Request $request)
  {
    $em =$this->getDoctrine()->getManager(); 
    $user = $this->get('security.token_storage')
    ->getToken()->getUser(); 
    if ($user != 'anon.' && $user->getRola() == 'ADMIN') {
      return $this->redirectToRoute('homepage_admin');
    }
    if ($user != 'anon.' && $user->getRola() == 'USER') {
      if ($user->getEvento()) {
        return $this->redirectToRoute('eventos_index');
      }else{
        $eventos = $em->getRepository('AppBundle:Evento')->findByUser($user); 
        if (count($eventos) == 0 ) {
          return $this->redirectToRoute('eventos_new');
        }
        if (count($eventos) == 1 ) {
          return $this->redirectToRoute('regalos_index',['id'=>$eventos[0]->getId()]);
        }
        if (count($eventos) > 1) {
          return $this->redirectToRoute('eventos_index');
        }
      }
    }
    return $this->redirectToRoute('homepage');
  }

  /**
   * @Route("emailView", name="emailView")
   */
  public function mailViewAction(Request $request)
  {
    return $this->render('AppBundle:Email:retiro.html.twig');
  }

  /**
   * @Route("test", name="test")
   */
  public function test(Request $request)
  {
    // set_time_limit(0);
    $em =$this->getDoctrine()->getManager(); 
    $ciudades = $em->getRepository('AppBundle:Ciudad')->findAll(); 
    foreach ($ciudades as $ciudad) {
      if (!$ciudad->getPais()) {
        $pais = $em->getRepository('AppBundle:Pais')->findOneByCodigo($ciudad->getCodigo());
        $ciudad->setPais($pais);
        $em->flush();
      }
    }
    return $this->render('AppBundle:Email:retiro.html.twig');
  }
}
