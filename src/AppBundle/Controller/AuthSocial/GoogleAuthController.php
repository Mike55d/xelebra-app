<?php

namespace AppBundle\Controller\AuthSocial;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User; // your user entity
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use KnpU\OAuth2ClientBundle\Client\Provider\GoogleClient;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use \Twig_Environment as Environment;

class GoogleAuthController extends SocialAuthenticator
{

	private $clientRegistry;
	private $em;
	private $router;
    public $encoder;
    public $mailer;
    private $twig;

	public function __construct(ClientRegistry $clientRegistry, EntityManagerInterface $em, RouterInterface $router , UserPasswordEncoderInterface $encoder , \Swift_Mailer $mailer , Environment $twig )
	{
		$this->clientRegistry = $clientRegistry;
		$this->em = $em;
		$this->router = $router;
        $this->encoder = $encoder;
        $this->mailer = $mailer;
        $this->twig   = $twig;

	}

	public function supports(Request $request)
	{
        // continue ONLY if the current ROUTE matches the check ROUTE
		return $request->attributes->get('_route') === 'connect_google_check';
	}

	public function getCredentials(Request $request)
	{
        // this method is only called if supports() returns true

        // For Symfony lower than 3.4 the supports method need to be called manually here:
        // if (!$this->supports($request)) {
        //     return null;
        // }

		return $this->fetchAccessToken($this->getGoogleClient());
	}

	public function getUser($credentials, UserProviderInterface $userProvider )
	{
		/** @var googleUser $googleUser */
		$googleUser = $this->getGoogleClient()
		->fetchUserFromToken($credentials);
		$email = $googleUser->getEmail();
        // 1) have they logged in with Facebook before? Easy!
		$existingUser = $this->em->getRepository(User::class)
		->findOneBy(['googleId' => $googleUser->getId()]);
		if ($existingUser) {
			return $existingUser;
		}

        // 2) do we have a matching user by email?
		$user = $this->em->getRepository(User::class)
		->findOneBy(['username' => $email]);
		if (!$user) $user = new User;

        // 3) Maybe you just want to "register" them by creating
        // a User object
        $pw = $this->encoder->encodePassword($user, $user->getUsername());
        $user->setPassword($pw);
		$user->setGoogleId($googleUser->getId());
		$user->setUsername($email);
		$user->setRoles("ROLE_USER");
		$user->setRola('USER');
		$user->setActive(1);
		$this->em->persist($user);
		$this->em->flush();
        $message = (new \Swift_Message('Notificacion'))
        ->setSubject('Notificaciones')
        ->setFrom('info@financeirocheznous.org')
        ->setTo($user->getUsername())
        ->setBody(
            $this->twig->render(
                'AppBundle:Email:Registro.html.twig',
                array('user' => $user)),'text/html');
        $this->mailer->send($message);

		return $user;
	}

    /**
     * @return FacebookClient
     */
    private function getGoogleClient()
    {
    	return $this->clientRegistry
            // "facebook_main" is the key used in config/packages/knpu_oauth2_client.yaml
    	->getClient('google');
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // change "app_homepage" to some route in your app
    	$targetUrl = $this->router->generate('home');

    	return new RedirectResponse($targetUrl);

        // or, on success, let the request continue to be handled by the controller
        //return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
    	$message = strtr($exception->getMessageKey(), $exception->getMessageData());

    	return new Response($message, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
    	return new RedirectResponse(
            '/login', // might be the site, where users choose their oauth provider
            Response::HTTP_TEMPORARY_REDIRECT
          );
    }

  }
