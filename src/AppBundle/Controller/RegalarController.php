<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Evento;
use AppBundle\Entity\RegalosEvento;
use AppBundle\Entity\Factura;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Core\ProductionEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use PayPalCheckoutSdk\Orders\OrdersAuthorizeRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use MercadoPago\SDK;
use MercadoPago\Preference;
use MercadoPago\Item;

/**
* @Route("{_locale}/regalar")
*/
class RegalarController extends Controller
{
  public $session;
  public function __construct(){
    $this->session = new Session();
  }
  /**
  * @Route("/", name="regalar_index")
  */
  public function indexAction(Request $request)
  {
    $em =$this->getDoctrine()->getManager();
    $tipoEventos = $em->getRepository('AppBundle:tipoEvento')->findAll();  
    $eventos = $em->getRepository('AppBundle:Evento')->findAll(); 
    if ($request->get('buscar') || $request->get('pais') || $request->get('tipo')) {
      $eventos = $em->getRepository('AppBundle:Evento')
      ->buscar($request->get('buscar') , $request->get('tipo') ,$request->get('pais'),null,null); 
    }
    $paises = $em->getRepository('AppBundle:Pais')->findAll();
    $paisesEvento = [];
    foreach ($paises as $pais) {
      $eventosPais = $em->getRepository('AppBundle:Evento')->findByPais($pais); 
      if (count($eventosPais)) {
        $paisesEvento[] = $pais;
      }
    }
    return $this->render('AppBundle:Regalar:index.html.twig', array(
     'eventos'=> $eventos,
     'palabra'=> $request->get('buscar'),
     'tipoEventos' => $tipoEventos,
     'paises' => $paisesEvento,
     'paisR' => $request->get('pais'),
     'tipoR' => $request->get('tipo'),
   ));
  }

  /**
  * @Route("/{id}/{titulo}/regalar", name="regalar_new")
  */
  public function regalarAction(Evento $evento,$titulo,Request $request)
  {
    $em =$this->getDoctrine()->getManager(); 
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $categorias = $em->getRepository('AppBundle:Categoria')->findAll(); 
    $regalos = $em->getRepository('AppBundle:Regalo')
    ->findBy(['tipo'=>$evento->getTipo(),'lista'=>$evento->getTipoLista()]); 
    if ($evento->getComPersonalizada()) {
      $impuestos = $evento->getImpuestoPais();
      $comisionInvitado = $evento->getPorcentInvitado();
    }else{
      $impuestos = $evento->getComision()->getImpuestoPais();
      $comisionInvitado = $evento->getComision()->getPorcentInvitado();
    }
    $monedaMenu = $em->getRepository('AppBundle:Conversion')
    ->findOneByMoneda($this->session->get('money')); 
    $monedaPais = $em->getRepository('AppBundle:Conversion')
    ->findOneByPais($this->session->get('country')[1]); 
    $buscar = $request->get('buscar') ?? '';
    $categoria = $request->get('categoria') ?? '';
    if ($request->get('categoria') || $request->get('buscar')) {
      $regalos = $em->getRepository('AppBundle:Regalo')->buscar($request->get('buscar'),$request->get('categoria')); 
    }
    return $this->render('AppBundle:Regalar:new.html.twig', array(
     'evento'=>$evento,
     'regalos'=> $regalos,
     'link' => $actual_link,
     'categorias' => $categorias,
     'buscar'=> $buscar,
     'category' => $categoria,
     'monedaMenu'=> $monedaMenu,
     'monedaPais' => $monedaPais,
     'impuestos' => $impuestos,
     'comisionInvitado' => $comisionInvitado
   ));
  }

  /**
  * @Route("/{id}/pagar", name="pagar")
  */
  public function pagarAction(Evento $evento ,Request $request)
  {
    $em =$this->getDoctrine()->getManager();
    $data = json_decode($request->get('data'),true);
    $tasaArgentina = $em->getRepository('AppBundle:Conversion')->findOneByMoneda('ARS'); 

    if ($evento->getComPersonalizada()) {
      $comision = $evento->getPorcentInvitado();
      $impuestoPais = $evento->getImpuestoPais();
    }else{
      $comision = $evento->getComision()->getPorcentInvitado();
      $impuestoPais = $evento->getComision()->getImpuestoPais();
    }
    $impuestos = ($data['total']* $comision)/100;
    $impuestosPais = ($data['total']* $impuestoPais)/100;
    $money = $em->getRepository('AppBundle:Conversion')
    ->findOneByMoneda($this->session->get('money')); 
    $total = $data['total'] + $impuestos + $impuestosPais;
    $conversion = $total;
    $conversionArgentina =  ($total/$money->getConversion()) * $tasaArgentina->getConversion();
    $mp = new SDK;
    $mp->setAccessToken('TEST-401144234505623-112519-8fffb8de37fc1acb4b0ad4acb2271fa2-493055437');
    $mp->setClientId('401144234505623');
    $mp->setClientSecret('q0H0s6yTgDryLXDazhTYgKIclizjIshv');
    $mpPreferences = new Preference;
// Crea un ítem en la preferencia
    $item = new Item;
    $item->title = 'Mi producto';
    $item->quantity = 1;
    $item->unit_price = $conversionArgentina;
    $mpPreferences->items = array($item);
    $mpPreferences->payment_methods = array(
      "installments" => 1
    );
    $mpPreferences->back_urls = array(
      "success" => "https://www.tu-sitio/success",
      "failure" => "http://www.tu-sitio/failure",
      "pending" => "http://www.tu-sitio/pending"
    );
    $mpPreferences->auto_return = "approved";
    $mpPreferences->binary_mode = true;
    $mpPreferences->save();
    $disclaimer = $em->getRepository('AppBundle:textoEditable')
    ->findOneBySeccion('disclaimer');
    $totalUsd = $total/ $money->getConversion();
    return $this->render('AppBundle:Regalar:pagar.html.twig', array(
      'productos'=>$data,
      'strProductos'=>$request->get('data'),
      'evento'=> $evento,
      'preference'=> $mpPreferences,
      'subtotal' => $data['total'],
      'impuestos'=>$impuestos,
      'conversion'=>$conversion,
      'money'=>$money->getMoneda(),
      'impuestosPais'=> $impuestosPais ,
      'total'=>$total,
      'comision'=>$comision,
      'conversionArgentina'=>$conversionArgentina,
      'moneda' => $money->getConversion(),
      'impuestoPais' =>$impuestoPais,
      'disclaimer' => $disclaimer,
      'totalUsd' => $totalUsd,
      'tasaArgentina' => $tasaArgentina
    ));
  }

   /**
    * @Route("/{order}/gracias", name="graciasPage")
    */
   public function graciasAction($order)
   {
    $em =$this->getDoctrine()->getManager();
    $money = $em->getRepository('AppBundle:Conversion')
    ->findOneByMoneda($this->session->get('money'));
    $factura = $em->getRepository('AppBundle:Factura')->findOneByPaypalOrder($order);
    $tasaArgentina = $em->getRepository('AppBundle:Conversion')->findOneByMoneda('ARS');
    $monedaMenu = $em->getRepository('AppBundle:Conversion')
    ->findOneByMoneda($this->session->get('money'));  
    $evento = $factura->getEvento();
    if ($evento->getComPersonalizada()) {
      $comision = $evento->getPorcentInvitado();
      $impuestoPais = $evento->getImpuestoPais();
    }else{
      $comision = $evento->getComision()->getPorcentInvitado();
      $impuestoPais = $evento->getComision()->getImpuestoPais();
    }

    $productos = $em->getRepository('AppBundle:RegalosEvento')->findByFactura($factura->getId());
    $subtotal = 0;
    foreach ($productos as $producto) {
      $subtotal+= $producto->getContribucion();
    }
    $impuestos = ($subtotal * $comision )/100;
    $impuestosPais = ($subtotal * $impuestoPais )/100;
    $totalImpuestos = $subtotal + $impuestos + $impuestosPais;
    return $this->render('AppBundle:Regalar:gracias.html.twig', array(
      'evento'=>$factura->getEvento(),
      'productos'=> $productos,
      'total'=> $factura->getTotal(),
      'subtotal'=> $subtotal,
      'impuestos'=> $impuestos,
      'money'=>$money->getMoneda(),
      'comision'=>$comision,
      'monedaMenu' => $monedaMenu,
      'moneda' => $money->getConversion(),
      'impuestoPais' =>$impuestoPais,
      'impuestosPais' => $impuestosPais,
      'totalImpuestos' => $totalImpuestos
    ));
  }

  /**
    * @Route("/paypal", name="paypal")
    */
  public function paypal(Request $request , \Swift_Mailer $mailer){
    // Creating an environment
    $em =$this->getDoctrine()->getManager(); 
    $money = $em->getRepository('AppBundle:Conversion')
    ->findOneByMoneda($this->session->get('money'));
    $clientId = "AS_hK-7zZ2UAsXieIcLFnLWNOgToeZdnthgJO7AGGaZdmN4qk2rnPCTZ2BetH_DUpqtXIArCk_QgqGSy";
    $clientSecret = "EGaJdM_KTzxHMrw4bKhSswojwUtHSTRp9WfiRZ0zwOAnuPJjdWBro0EsmSZyGy6HdUtjvc6kJTWj6ghf";
    $environment = new SandboxEnvironment($clientId, $clientSecret);
    $client = new PayPalHttpClient($environment);
    $evento = $em->getRepository('AppBundle:Evento')->find($request->get('evento')); 
    if ($request->get('nombre')) {
// Here, OrdersCaptureRequest() creates a POST request to /v2/checkout/orders
// $response->result->id gives the orderId of the order created above
      $paypalOrder = new OrdersCaptureRequest($request->get('order'));
      $paypalOrder->prefer('return=representation');
      $paypalOrder->body = "{}";
      try {
// Call API with your client and get a response for your call
        $response = $client->execute($paypalOrder);
        $monto =intval(round($response->result->purchase_units[0]->amount->value),2);
        $order = $em->getRepository('AppBundle:Factura')->findByPaypalOrder($request->get('order')); 
        if (!$order) {
          $productos = json_decode($request->get('data'),true);
          $factura = new Factura;
          $factura->setEvento($evento);
          $factura->setFecha(new \Datetime());
          $factura->setMensaje($request->get('mensaje'));
          $factura->setPaypalOrder($request->get('order'));
          $factura->setMetodoPago('Paypal');
          $factura->setTotal(0);
          $em->persist($factura);
          $em->flush();
          if ($evento->getComPersonalizada()) {
            $comision = $evento->getPorcentInvitado();
            $impuestoPais = $evento->getImpuestoPais();
          }else{
            $comision = $evento->getComision()->getPorcentInvitado();
            $impuestoPais = $evento->getComision()->getImpuestoPais();
          }

          $regalos = [];
          $totalContribuir = 0;
          foreach ($productos['productos'] as $producto) {
            $regalo = $em->getRepository('AppBundle:Regalo')->find($producto['id']); 
            $regaloEvento = new RegalosEvento;
            $regaloEvento->setRegalo($regalo);
            $regaloEvento->setEvento($evento);
            $regaloEvento->setNombre($request->get('nombre'));
            $regaloEvento->setApellido($request->get('apellido'));
            $regaloEvento->setEmail($request->get('email'));
            $regaloEvento->setMensaje($request->get('mensaje'));
            $regaloEvento->setContribucion($producto['contribuir'] / $money->getConversion() ?? 250);
            $totalContribuir = $totalContribuir + (intval($producto['contribuir']));
            $regaloEvento->setFactura($factura);
            $em->persist($regaloEvento);
            $em->flush();
            $regalos[]=$regaloEvento;
          }
          $totalContribuirMoney = $totalContribuir / $money->getConversion();
          $impuestos = ($totalContribuirMoney* $comision)/100;
          $impuestosPais = ($totalContribuirMoney* $impuestoPais)/100;
          $total = $totalContribuirMoney + $impuestos + $impuestosPais;
          $factura->setTotal($total);
          $em->flush();

          $evento->setContribucion($evento->getContribucion() + $totalContribuirMoney);
          $user = $evento->getUser();
          $evento->setSaldo($evento->getSaldo() + $totalContribuirMoney);
          $em->flush();
          $message = (new \Swift_Message('Notificacion'))
          ->setSubject('Recibiste regalos de '.$request->get('nombre'))
          ->setFrom('info@financeirocheznous.org')
          ->setTo($evento->getEmail())
          ->setBody(
            $this->renderView(
              'AppBundle:Email:recibisteRegalos.html.twig',
              array('regalos' => $regalos, 'evento'=>$evento)),'text/html');
          $mailer->send($message);
          return $this->redirectToRoute('graciasPage',[
            'order'=> $request->get('order')
          ]);
        }
// If call returns body in response, you can get the deserialized version from the result attribute of the response
      }catch (HttpException $ex) {
        return $this->redirectToRoute('graciasPage',[
          'order'=> $request->get('order')
        ]);
      }

    }

  }
  /**
    * @Route("/mercadopago", name="mercadopago")
    */
  public function mercadopago(Request $request , \Swift_Mailer $mailer){
    $em =$this->getDoctrine()->getManager(); 
    $evento = $em->getRepository('AppBundle:Evento')->find($request->get('evento')); 
    $order = $em->getRepository('AppBundle:Factura')->findByPaypalOrder($request->get('payment_id')); 
    $money = $em->getRepository('AppBundle:Conversion')
    ->findOneByMoneda($this->session->get('money')); 
    $monedaArgentina = $em->getRepository('AppBundle:Conversion')
      ->findOneByMoneda('ARS');
    if (!$order) {
      $productos = json_decode($request->get('data'),true);
      $factura = new Factura;
      $factura->setEvento($evento);
      $factura->setFecha(new \Datetime());
      $factura->setMensaje($request->get('mensaje'));
      $factura->setMetodoPago('MercadoPago');
      $factura->setPaypalOrder($request->get('payment_id'));
      $factura->setTotal(0);
      $em->persist($factura);
      $em->flush();
      if ($evento->getComPersonalizada()) {
        $comision = $evento->getPorcentInvitado();
        $impuestoPais = $evento->getImpuestoPais();
      }else{
        $comision = $evento->getComision()->getPorcentInvitado();
        $impuestoPais = $evento->getComision()->getImpuestoPais();
      }

      $regalos = [];
      $totalContribuir = 0;
      foreach ($productos['productos'] as $producto) {
        $regalo = $em->getRepository('AppBundle:Regalo')->find($producto['id']); 
        $regaloEvento = new RegalosEvento;
        $regaloEvento->setRegalo($regalo);
        $regaloEvento->setEvento($evento);
        $regaloEvento->setNombre($request->get('nombre'));
        $regaloEvento->setApellido($request->get('apellido'));
        $regaloEvento->setEmail($request->get('email'));
        $regaloEvento->setMensaje($request->get('mensaje'));
        $regaloEvento->setContribucion(($producto['contribuir'] / $money->getConversion()) * $monedaArgentina->getConversion()?? 250);
        $totalContribuir = $totalContribuir + (intval($producto['contribuir']));
        $regaloEvento->setFactura($factura);
        $em->persist($regaloEvento);
        $em->flush();
        $regalos[]=$regaloEvento;
      }
      $totalContribuirMoney = $totalContribuir / $money->getConversion();

      $impuestos = ($totalContribuirMoney * $comision)/100;
      $impuestosPais = ($totalContribuirMoney * $impuestoPais)/100;
      
      
      $total = $totalContribuirMoney + $impuestos + $impuestosPais;

      
      $factura->setTotal($total * intval($monedaArgentina->getConversion()));
      $em->flush();

      $message = (new \Swift_Message('Notificacion'))
      ->setSubject('Recibiste regalos de '.$request->get('nombre'))
      ->setFrom('info@financeirocheznous.org')
      ->setTo($evento->getEmail())
      ->setBody(
        $this->renderView(
          'AppBundle:Email:recibisteRegalos.html.twig',
          array('regalos' => $regalos, 'evento'=>$evento)),'text/html');
      $mailer->send($message);
      $evento->setContribucion($evento->getContribucion() + ($totalContribuirMoney * intval($monedaArgentina->getConversion())));
      $user = $evento->getUser();
      $evento->setSaldo($evento->getSaldo() + ($totalContribuirMoney * intval($monedaArgentina->getConversion())));
      $em->flush();
      return $this->redirectToRoute('graciasPage',[
        'order'=> $request->get('payment_id')
      ]);
    } 
  }

    /**
  * @Route("/compartirMail", name="compartirMail")
  */
    public function compartirMail(Request $request ,\Swift_Mailer $mailer){
      $em =$this->getDoctrine()->getManager(); 
      $mail = $request->get('email');
      $evento = $em->getRepository('AppBundle:Evento')
      ->find($request->get('evento'));
      $message = (new \Swift_Message('Notificacion'))
      ->setSubject('No te pierdas el evento de  '.$evento->getNombre().' '.$evento->getApellido())
      ->setFrom('info@financeirocheznous.org')
      ->setTo($mail)
      ->setBody(
        $this->renderView(
          'AppBundle:Email:mailCompartir.html.twig',
          array('evento' => $evento)),'text/html');
      $mailer->send($message); 
      return $this->render('AppBundle:Regalar:mailEnviado.html.twig',[
        'evento'=> $evento
      ]);
    }

//   /**
//   * @Route("/getPreferences", name="getPreferences")
//   */
//   public function getPreferences(Request $request){

//     return new JsonResponse(json_encode(1));
//   }

//   /**
//   * @Route("/mpButton", name="mpButton")
//   */
//   public function mpButton(Request $request){

//    $mp = new SDK;
//     $mp->setAccessToken('TEST-401144234505623-112519-8fffb8de37fc1acb4b0ad4acb2271fa2-493055437');
//     $mp->setClientId('401144234505623');
//     $mp->setClientSecret('q0H0s6yTgDryLXDazhTYgKIclizjIshv');
//     $mpPreferences = new Preference;
// // Crea un ítem en la preferencia
//     $item = new Item;
//     $item->title = 'Mi producto';
//     $item->quantity = 1;
//     $item->unit_price = '3612736.667';
//     $mpPreferences->items = array($item);
//     $mpPreferences->payment_methods = array(
//       "installments" => 1
//     );
//     $mpPreferences->back_urls = array(
//       "success" => "https://www.tu-sitio/success",
//       "failure" => "http://www.tu-sitio/failure",
//       "pending" => "http://www.tu-sitio/pending"
//     );
//     $mpPreferences->auto_return = "approved";
//     $mpPreferences->binary_mode = true;
//     $mpPreferences->save();

//     return $this->render('AppBundle:Regalar:mpButton.html.twig',[
//       'preference'=> $mpPreferences
//     ]);
//   }

  }
