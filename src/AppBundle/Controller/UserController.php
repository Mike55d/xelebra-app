<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserController extends Controller
{
/**
* @Route("{_locale}/register" , name="users_register")
*/
public function registerAction(Request $request , \Swift_Mailer $mailer)
{
	$em =$this->getDoctrine()->getManager();
	if ($request->get('email')) {
		$user = $em->getRepository('AppBundle:User')
		->findByUsername($request->get('email')); 
		if ($user) {
			$error = 'Este email ya esta registrado';
			return $this->render('AppBundle:User:register.html.twig', array(
				'error'=>$error
			));
		}
		$user = new User;
		$user->setUsername($request->get('email'));
		$user->setRoles("ROLE_USER");
		$user->setRola("USER");
		$user->setActive(1);
		$password = $this->get('security.password_encoder')
		->encodePassword($user,$request->get('pw'));
		$user->setPassword($password);
		$em->persist($user);
		$em->flush();
		$message = (new \Swift_Message('Notificacion'))
		->setSubject('Notificaciones')
		->setFrom('info@financeirocheznous.org')
		->setTo($user->getUsername())
		->setBody(
			$this->renderView(
				'AppBundle:Email:Registro.html.twig',
				array('user' => $user)),'text/html');
		$mailer->send($message);
		$token = new UsernamePasswordToken(
			$user,
			$password,
			'admin',
			$user->getRoles()
		);

		$this->get('security.token_storage')->setToken($token);
		$this->get('session')->set('_security_main', serialize($token));

		$this->addFlash('success', 'You are now successfully registered!');
		return $this->redirectToRoute('home');
		// return $user;
		// return $this->get('security.authentication.guard_handler')
		// ->authenticateUserAndHandleSuccess(
		// 	$user,
		// 	$request,
		// 	$this->get('app.security.login_form_authenticator'),
		// 	'admin'
		// );
		// $this->addFlash('notice','usuario registrado correctamente');
		// return $this->redirectToRoute('homepage');
	} 
	return $this->render('AppBundle:User:register.html.twig', array(
		'error'=> null
	));


}
	/**
	* @Route("{_locale}/{pass}/resetPw" , name="users_resetPw" , requirements={"pass"=".+"})
	*/
	public function resetPw($pass, Request $request){
		$em =$this->getDoctrine()->getManager(); 
		$user = $em->getRepository('AppBundle:User')
		->findOneByPassword($pass); 
		if ($request->get('pw')) {
			$password = $this->get('security.password_encoder')
			->encodePassword($user,$request->get('pw'));
			$user->setPassword($password);
			$em->flush();
			return $this->render('AppBundle:User:resetPwSuccess.html.twig');
		}
		return $this->render('AppBundle:User:resetPw.html.twig',[
			'message' => '',
			'user'=> $user,
		]);
	}

/**
	* @Route("{_locale}/forgotPw" , name="users_forgotPw")
	*/
	public function forgotPw(Request $request, \Swift_Mailer $mailer){
		$em =$this->getDoctrine()->getManager(); 
		if ($request->get('email')) {
			$user = $em->getRepository('AppBundle:User')
			->findOneByUsername($request->get('email')); 
			if ($user) {
				$mensaje = 'Email enviado';
				$message = (new \Swift_Message('Notificacion'))
				->setSubject('Notificaciones')
				->setFrom('info@financeirocheznous.org')
				->setTo($user->getUsername())
				->setBody(
					$this->renderView(
						'AppBundle:Email:forgotPw.html.twig',
						array('user' => $user)),'text/html');
				$mailer->send($message);
			}else{
				$mensaje = 'Usuario no existe';
			}
			return $this->render('AppBundle:User:forgotPw.html.twig',[
				'message' => $mensaje
			]);
		}
		return $this->render('AppBundle:User:forgotPw.html.twig',[
			'message' => ''
		]);

	}

/**
	* @Route("{_locale}/editarPerfil" , name="users_editarPerfil")
	*/
	public function editarPerfil(Request $request){
		$em =$this->getDoctrine()->getManager(); 
		$user = $this->get('security.token_storage')
		->getToken()->getUser(); 
		$eventos = $em->getRepository('AppBundle:Evento')->findByUser($user);
		$panelNovio = count($eventos) > 1 ? false: true;
		if ($request->get('nombre')) {
			$user->setNombre($request->get('nombre'));
			$user->setApellido($request->get('apellido'));
			$em->flush();
			return $this->render('AppBundle:User:perfilEditado.html.twig');
		}
		return $this->render('AppBundle:User:editarPerfil.html.twig',[
			'user' => $user,
			'panelNovio' => $panelNovio,
			'eventoId' => $eventos[0]->getId(),
		]);
	}


}
