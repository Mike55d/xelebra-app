<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Evento;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


/**
* @Route("{_locale}/eventos")
*/
class EventosController extends Controller
{
  /**
   * @Route("/", name="eventos_index")
   */
  public function index()
  {
    $em =$this->getDoctrine()->getManager(); 
    $user = $this->get('security.token_storage')
    ->getToken()->getUser(); 
    $listas = $em->getRepository('AppBundle:Evento')->findByUser($user->getId());
    $evento = $user->getEvento(); 
    return $this->render('AppBundle:Eventos:index.html.twig', [
     'listas'=>$listas,
     'evento' => $evento,
   ]);
  }

  /**
   * @Route("/new", name="eventos_new")
   */
  public function new(Request $request , \Swift_Mailer $mailer)
  {
    $em =$this->getDoctrine()->getManager(); 
    $user = $this->get('security.token_storage')
    ->getToken()->getUser(); 
    $tipos = $em->getRepository('AppBundle:tipoEvento')->findAll(); 
    $paises = $em->getRepository('AppBundle:Pais')->findBy([],['pais'=>'ASC']); 
    $session = new Session();
    $paisSelected = $session->get('country');
    if ($request->get('nombre')) {
      $evento = new Evento;
      $evento->setTitulo($request->get('titulo'));
      $tipo = $em->getRepository('AppBundle:tipoEvento')->find($request->get('tipo')); 
      $evento->setTipo($tipo);
      $evento->setComPersonalizada(0);
      $evento->setSaldo(0);
      $evento->setTipoCobro($request->get('tipoCobro'));
      $evento->setNombre($request->get('nombre'));
      $evento->setApellido($request->get('apellido'));
      $evento->setEmail($request->get('email'));

      $evento->setNombre2($request->get('nombre2'));
      $evento->setApellido2($request->get('apellido2'));
      $evento->setEmail2($request->get('email2'));

      $evento->setLugarEvento1($request->get('lugar1'));
      $evento->setMapa1($request->get('mapa1'));
      $evento->setLugarEvento2($request->get('lugar2'));
      $evento->setMapa2($request->get('mapa2'));
      $evento->setLugarEvento3($request->get('lugar3'));
      $evento->setMapa3($request->get('mapa3'));

      $user->setApellido2($request->get('apellido'));
      $user->setEmail2($request->get('email2'));
      $user->setNombre2($request->get('nombre2'));
      $evento->setFecha(new \Datetime($request->get('fecha')));
      $evento->setInvitados($request->get('invitados'));
      $comision = $em->getRepository('AppBundle:Comisiones')
      ->findOneBy(['pais'=>$paisSelected[1],'tipoEvento'=>$tipo->getId()]); 
      $evento->setComision($comision);
      $pais = $em->getRepository('AppBundle:Pais')->find($request->get('pais')); 
      $evento->setPais($pais);
      $evento->setTipoLista($request->get('tipoLista'));
      $evento->setCiudad($request->get('ciudad'));
      if (!$request->get('about')) {
        $evento->setAbout('¡Nos casamos y estamos muy contentos de poder compartir nuestra alegría con todos ustedes! Acá van a encontrar toda la información del casamiento. ¡Los esperamos!');
      }else{
        $evento->setAbout($request->get('about'));
      }
      $evento->setuser($user);
      if ($request->files->get('image')) {
        $file = $request->files->get('image');
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move($this->getParameter('images'),$fileName);
        $evento->setImage($fileName);
      }else{
        $evento->setImage('default.jpg');
      }
      $em->persist($evento);
      $em->flush();

      $newUserExist = $em->getRepository('AppBundle:User')->findOneByUsername($request->get('email2'));
      if (!$newUserExist) {
        $newUser = new User;
        $newUser->setName($request->get('nombre2'));
        $password = $this->get('security.password_encoder')
        ->encodePassword($user,'1234');
        $newUser->setPassword($password);
        $newUser->setActive(1);
        $newUser->setUsername($request->get('email2'));
        $newUser->setNombre($request->get('nombre2'));
        $newUser->setApellido($request->get('apellido2'));
        $newUser->setEmail($request->get('email2'));
        $newUser->setRoles("ROLE_USER");
        $newUser->setRola("USER");
        $newUser->setUser($user);
        $newUser->setEvento($evento);
        $em->persist($newUser);
        $em->flush();
      }

      $message = (new \Swift_Message('Notificacion'))
      ->setSubject('Felicidades creaste tu lista de regalos')
      ->setFrom('info@financeirocheznous.org')
      ->setTo($evento->getEmail())
      ->setBody(
        $this->renderView(
          'AppBundle:Email:listaCreada.html.twig',
          array('evento' => $evento)),'text/html');
      $mailer->send($message);
      $message = (new \Swift_Message('Notificacion'))
      ->setSubject('Felicidades creaste tu lista de regalos')
      ->setFrom('info@financeirocheznous.org')
      ->setTo($evento->getEmail2())
      ->setBody(
        $this->renderView(
          'AppBundle:Email:listaCreada.html.twig',
          array('evento' => $evento)),'text/html');
      $mailer->send($message);
      return $this->redirectToRoute('eventos_share',[
        'id'=>$evento->getId(),
        'nombre' => $evento->getTitulo()
      ]);
    }
    return $this->render('AppBundle:Eventos:new.html.twig', [
      'paises' => $paises,
      'tipos'=> $tipos
    ]);
  }

  /**
   * @Route("/{id}/edit", name="eventos_edit")
   */
  public function edit(Request $request , Evento $evento)
  {
    $em =$this->getDoctrine()->getManager();
    $paises = $em->getRepository('AppBundle:Pais')->findBy([],['pais'=>'ASC']);  
    $ciudades = $em->getRepository('AppBundle:Ciudad')->findByCodigo($evento->getPais()->getCodigo(),['ciudad'=>'ASC']);  
    if ($request->get('nombre')) {
      $evento->setTitulo($request->get('titulo'));
      $evento->setNombre($request->get('nombre'));
      $evento->setApellido($request->get('apellido'));
      $evento->setEmail($request->get('email'));
      if ($request->get('email2')) {
        $user2 = $em->getRepository('AppBundle:User')->findOneByUsername($evento->getEmail2());
        $user2Exist = $em->getRepository('AppBundle:User')->findOneByEmail($request->get('email2')); 
        if (!$user2Exist) {
          $user2->setUsername($request->get('email2'));
          $user2->setNombre($request->get('nombre2'));
          $user2->setApellido($request->get('apellido2'));
          $user2->setEmail($request->get('email2'));
          $em->flush();
        }
      }
      $evento->setNombre2($request->get('nombre2'));
      $evento->setApellido2($request->get('apellido2'));
      $evento->setEmail2($request->get('email2'));

      $evento->setFecha(new \Datetime($request->get('fecha')));
      $evento->setInvitados($request->get('invitados'));
      $pais = $em->getRepository('AppBundle:Pais')->find($request->get('pais')); 
      $evento->setPais($pais);
      $evento->setCiudad($request->get('ciudad'));
      $evento->setAbout($request->get('about'));
      if ($file = $request->files->get('image')) {
        $file = $request->files->get('image');
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move($this->getParameter('images'),$fileName);
        $evento->setImage($fileName);
      }
      $em->flush();
      return $this->redirectToRoute('regalos_index',['id'=>$evento->getId()]);
    }
    return $this->render('AppBundle:Eventos:edit.html.twig', [
     'evento' => $evento,
     'paises' => $paises,
     'ciudades' => $ciudades
   ]);
  }

    /**
     * @Route("/{id}/del", name="eventos_del")
     */
    public function delete(Request $request)
    {
      return $this->render('AppBundle:Eventos:del.html.twig', [

      ]);
    }

  /**
   * @Route("/{id}/{nombre}/share", name="eventos_share")
   */
  public function shareAction(Evento $evento , Request $request , \Swift_Mailer $mailer)
  {
    $em =$this->getDoctrine()->getManager(); 
    $actual_link = "http://$_SERVER[HTTP_HOST]/es/regalar/".$evento->getId().'/'.str_replace(' ','',$evento->getTitulo()).'/'.'regalar';
    if ($request->get('mails')) {
      $mails = explode(',',$request->get('mails'));
      foreach ($mails as $mail) {
        $message = (new \Swift_Message('Notificacion'))
        ->setSubject('No te pierdas el evento de  '.$evento->getNombre().' '.$evento->getApellido())
        ->setFrom('info@financeirocheznous.org')
        ->setTo($mail)
        ->setBody(
          $this->renderView(
            'AppBundle:Email:mailCompartir.html.twig',
            array('evento' => $evento)),'text/html');
        $mailer->send($message);
      }
      return $this->render('AppBundle:Eventos:mailEnviado.html.twig',[
        'evento' => $evento,
      ]);
    }
    return $this->render('AppBundle:Eventos:share.html.twig', [
      'link'=> $actual_link,
      'evento'=> $evento
    ]);
  }

  /**
   * @Route("/terminosYCondiciones", name="terminosYCondiciones")
   */
  public function terminosYCondiciones(Request $request)
  {
    return $this->render('AppBundle:Eventos:terminosYCondiciones.html.twig', [

    ]);
  }

  /**
   * @Route("/getCiudades", name="getCiudades")
   */
  public function getCiudades(Request $request)
  {

    $em =$this->getDoctrine()->getManager(); 
    $pais = $em->getRepository('AppBundle:Pais')->find($request->get('pais'));
    $ciudades = $em->getRepository('AppBundle:Ciudad')->findByCodigo($pais->getCodigo() ,['ciudad'=>'ASC']); 
    $normalizer = new ObjectNormalizer();
    $encoder = new JsonEncoder();
    $serializer = new Serializer([$normalizer], [$encoder]);
    $citys =$serializer->serialize($ciudades, 'json'); 
    return new JsonResponse($citys);
  }



}
