<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class NavbarController extends Controller
{
	public $session;
	public function __construct(){
		$this->session = new Session();
	}
	public function countryAndMoneyAction()
	{
		$em =$this->getDoctrine()->getManager(); 
		$this->session->get('country');

		if (!$this->session->get('country')) {
			$this->session->set('country', ['Estados Unidos',34]);
		}
		if (!$this->session->get('money')) {
			$this->session->set('money', 'USD');
		}
		$country = $this->session->get('country');
		$money = $this->session->get('money');
		$cash = $em->getRepository('AppBundle:Conversion')->findAll(); 
		return $this->render('AppBundle:Navbar:Navbar.html.twig',[
			'country'=> $country,
			'money'=> $money,
			'cash'=> $cash,
		]);
	}
	/**
 * @Route("{country}/{id}/changeCountry" , name="changeCountry")
 */
	public function changeCountryAction($country,$id,Request $request)
	{	
		$em =$this->getDoctrine()->getManager();
		$this->session->set('country', [$country,$id]);
		$conversion = $em->getRepository('AppBundle:Conversion')->findOneByPais($id); 
		$this->session->set('money' , $conversion->getMoneda());
		$url = $_SERVER['HTTP_REFERER'];
		$newUrl = str_replace('pagar', 'mievento/'.'regalar', $url);
    		//return ([$newUrl,$locale,$lang]);
		return  $this->redirect($newUrl);
	}

	/**
 * @Route("{money}/changeMoney" , name="changeMoney")
 */
	public function changeMoneyAction($money,Request $request)
	{		
		$em =$this->getDoctrine()->getManager(); 
		$this->session->set('money', $money);
		$conversion = $em->getRepository('AppBundle:Conversion')->findOneByMoneda($money); 
		$this->session->set('country' ,[$conversion->getPais()->getPais() , $conversion->getPais()->getId()]);
		$url = $_SERVER['HTTP_REFERER'];
		$newUrl = str_replace('pagar', 'mievento/'.'regalar', $url);
    		//return ([$newUrl,$locale,$lang]);
		return  $this->redirect($newUrl);
	}

}
