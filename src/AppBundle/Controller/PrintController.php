<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Spipu\Html2Pdf\Html2Pdf;
use MercadoPago\SDK;
use MercadoPago\Preference;
use MercadoPago\Item;

class PrintController extends Controller
{
  /**
   * @Route("/printIngles" , name="print_ingles")
   */
  public function PrintInglesAction()
  {
  	$html2pdf = new Html2Pdf();
  	$html2pdf->writeHTML($this->renderView('AppBundle:Print:ingles.html.twig',[
  	]));

  	$html2pdf->output('xelebra.pdf', 'D');
  }

  /**
   * @Route("/printEspañol" , name="print_español")
   */
  public function PrintEspañolAction()
  {
  	$html2pdf = new Html2Pdf();
  	$html2pdf->writeHTML($this->renderView('AppBundle:Print:español.html.twig',[
  	]));

  	$html2pdf->output('xelebra.pdf', 'D');
  }


  /**
   * @Route("/testMp" , name="testMp")
   */
  public function testMp()
  {
    $mp = new SDK;
    $mp->setAccessToken('TEST-401144234505623-112519-8fffb8de37fc1acb4b0ad4acb2271fa2-493055437');
    $mp->setClientId('401144234505623');
    $mp->setClientSecret('q0H0s6yTgDryLXDazhTYgKIclizjIshv');
    $mpPreferences = new Preference;
// Crea un ítem en la preferencia
    $item = new Item;
    $item->title = 'Mi producto';
    $item->quantity = 1;
    $item->unit_price = 12333;
    $mpPreferences->items = array($item);
    $mpPreferences->payment_methods = array(
      "installments" => 1
    );
    $mpPreferences->back_urls = array(
      "success" => "https://www.tu-sitio/success",
      "failure" => "http://www.tu-sitio/failure",
      "pending" => "http://www.tu-sitio/pending"
    );
    $mpPreferences->auto_return = "approved";
    $mpPreferences->binary_mode = true;
    $mpPreferences->save();
    return $this->render('AppBundle:Print:new.html.twig',[
      'preference' => $mpPreferences
    ]);
  }

  }
