<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends Controller
{
    /**
     * @Route("/{_locale}/login" , name="login")
     */
    public function loginAction(Request $request)
    {
    	$authenticationUtils = $this->get('security.authentication_utils');
		// get the login error if there is one
    	$error = $authenticationUtils->getLastAuthenticationError();
		// last username entered by the user
    	$lastUsername = $authenticationUtils->getLastUsername();
    	return $this->render(
    		'AppBundle:Security:login2.html.twig',
    		array(
			// last username entered by the user
    			'last_username' => $lastUsername,
    			'error' => $error,
    		)
    	);

    }

    /**
     * @Route("/{_locale}/fail" , name="fail")
     */
    public function failureAction()
    {
        return $this->render('AppBundle:Security:failure.html.twig',[

        ]);
    }

    /**
     * @Route("/{_locale}/set" , name="set")
     */
    public function setAction()
    {
        $em =$this->getDoctrine()->getManager();
        $regalos = $em->getRepository('AppBundle:Regalo')->findByLista(' clasica');
        foreach ($regalos as $regalo) {
            $regalo->setLista('clasica');
            $em->flush();
        } 
    }

}
