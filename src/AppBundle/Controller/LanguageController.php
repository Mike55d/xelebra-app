<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class LanguageController extends Controller
{
    /**
     * @Route("{_locale}/{lang}/changeLanguage" , name="changeLanguage")
     */
    public function indexAction($lang,Request $request)
    {
    		$url = $_SERVER['HTTP_REFERER'];
            $url2 = $_SERVER['HTTP_REFERER'];
    		$locale = '/'.$request->getLocale().'/';
    		$newUrl = str_replace($locale,'/'.$lang.'/',$url);
            $newUrl2 = str_replace('pagar', 'regalar', $newUrl);
    		//return ([$newUrl,$locale,$lang]);
    		return  $this->redirect($newUrl2);
    }

}
