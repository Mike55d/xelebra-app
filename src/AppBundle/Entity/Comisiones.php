<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comisiones
 *
 * @ORM\Table(name="comisiones")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ComisionesRepository")
 */
class Comisiones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="Pais")
    * @ORM\JoinColumn(name="pais", referencedColumnName="id")
    */

    private $pais;

    /**
    * @ORM\ManyToOne(targetEntity="tipoEvento")
    * @ORM\JoinColumn(name="tipoEvento", referencedColumnName="id")
    */
    private $tipoEvento;

    /**
     * @var string
     *
     * @ORM\Column(name="fijo", type="string", length=255)
     */
    private $fijo;

        /**
     * @var string
     *
     * @ORM\Column(name="fijoNovio", type="string", length=255)
     */
    private $fijoNovio;

        /**
     * @var string
     *
     * @ORM\Column(name="fijoInvitado", type="string", length=255)
     */
    private $fijoInvitado;

        /**
     * @var string
     *
     * @ORM\Column(name="impuestoPais", type="string", length=255)
     */
    private $impuestoPais;

    /**
     * @var string
     *
     * @ORM\Column(name="porcentNovio", type="string", length=255)
     */
    private $porcentNovio;

    /**
     * @var string
     *
     * @ORM\Column(name="porcentInvitado", type="string", length=255)
     */
    private $porcentInvitado;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fijo
     *
     * @param string $fijo
     *
     * @return Comisiones
     */
    public function setFijo($fijo)
    {
        $this->fijo = $fijo;

        return $this;
    }

    /**
     * Get fijo
     *
     * @return string
     */
    public function getFijo()
    {
        return $this->fijo;
    }

    /**
     * Set porcentNovio
     *
     * @param string $porcentNovio
     *
     * @return Comisiones
     */
    public function setPorcentNovio($porcentNovio)
    {
        $this->porcentNovio = $porcentNovio;

        return $this;
    }

    /**
     * Get porcentNovio
     *
     * @return string
     */
    public function getPorcentNovio()
    {
        return $this->porcentNovio;
    }

    /**
     * Set porcentInvitado
     *
     * @param string $porcentInvitado
     *
     * @return Comisiones
     */
    public function setPorcentInvitado($porcentInvitado)
    {
        $this->porcentInvitado = $porcentInvitado;

        return $this;
    }

    /**
     * Get porcentInvitado
     *
     * @return string
     */
    public function getPorcentInvitado()
    {
        return $this->porcentInvitado;
    }

    /**
     * Set pais
     *
     * @param \AppBundle\Entity\Pais $pais
     *
     * @return Comisiones
     */
    public function setPais(\AppBundle\Entity\Pais $pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return \AppBundle\Entity\Pais
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set tipoEvento
     *
     * @param \AppBundle\Entity\TipoEvento $tipoEvento
     *
     * @return Comisiones
     */
    public function setTipoEvento(\AppBundle\Entity\TipoEvento $tipoEvento = null)
    {
        $this->tipoEvento = $tipoEvento;

        return $this;
    }

    /**
     * Get tipoEvento
     *
     * @return \AppBundle\Entity\TipoEvento
     */
    public function getTipoEvento()
    {
        return $this->tipoEvento;
    }

    /**
     * Set fijoNovio
     *
     * @param string $fijoNovio
     *
     * @return Comisiones
     */
    public function setFijoNovio($fijoNovio)
    {
        $this->fijoNovio = $fijoNovio;

        return $this;
    }

    /**
     * Get fijoNovio
     *
     * @return string
     */
    public function getFijoNovio()
    {
        return $this->fijoNovio;
    }

    /**
     * Set fijoInvitado
     *
     * @param string $fijoInvitado
     *
     * @return Comisiones
     */
    public function setFijoInvitado($fijoInvitado)
    {
        $this->fijoInvitado = $fijoInvitado;

        return $this;
    }

    /**
     * Get fijoInvitado
     *
     * @return string
     */
    public function getFijoInvitado()
    {
        return $this->fijoInvitado;
    }

    /**
     * Set impuestoPais
     *
     * @param string $impuestoPais
     *
     * @return Comisiones
     */
    public function setImpuestoPais($impuestoPais)
    {
        $this->impuestoPais = $impuestoPais;

        return $this;
    }

    /**
     * Get impuestoPais
     *
     * @return string
     */
    public function getImpuestoPais()
    {
        return $this->impuestoPais;
    }
}
