<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Conversion
 *
 * @ORM\Table(name="conversion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConversionRepository")
 */
class Conversion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="Pais")
    * @ORM\JoinColumn(name="pais", referencedColumnName="id")
    */
    private $pais;

    /**
     * @var string
     *
     * @ORM\Column(name="idioma", type="string", length=255)
     */
    private $idioma;

    /**
     * @var string
     *
     * @ORM\Column(name="moneda", type="string", length=255)
     */
    private $moneda;

    /**
     * @var string
     *
     * @ORM\Column(name="conversion", type="string", length=255)
     */
    private $conversion;

/**
     * @var date
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private  $fecha;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idioma
     *
     * @param string $idioma
     *
     * @return Conversion
     */
    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma
     *
     * @return string
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * Set moneda
     *
     * @param string $moneda
     *
     * @return Conversion
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Get moneda
     *
     * @return string
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * Set conversion
     *
     * @param string $conversion
     *
     * @return Conversion
     */
    public function setConversion($conversion)
    {
        $this->conversion = $conversion;

        return $this;
    }

    /**
     * Get conversion
     *
     * @return string
     */
    public function getConversion()
    {
        return $this->conversion;
    }

    /**
     * Set pais
     *
     * @param \AppBundle\Entity\Pais $pais
     *
     * @return Conversion
     */
    public function setPais(\AppBundle\Entity\Pais $pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return \AppBundle\Entity\Pais
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Conversion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}
