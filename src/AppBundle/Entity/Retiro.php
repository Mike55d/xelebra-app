<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Retiro
 *
 * @ORM\Table(name="retiro")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RetiroRepository")
 */
class Retiro
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user", referencedColumnName="id")
    */
    private $user;

    /**
    * @ORM\ManyToOne(targetEntity="Evento")
    * @ORM\JoinColumn(name="evento", referencedColumnName="id")
    */
    private $evento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var int
     *
     * @ORM\Column(name="monto", type="integer")
     */
    private $monto;

     /**
     * @var string
     *
     * @ORM\Column(name="metodo", type="string")
     */
    private $metodo;

     /**
     * @var string
     *
     * @ORM\Column(name="status", type="string")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string" , nullable=true)
     */
    private $dni;

    /**
     * @var string
     *
     * @ORM\Column(name="cuit", type="string" , nullable=true)
     */
    private $cuit;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreBanco", type="string" , nullable=true)
     */
    private $nombreBanco;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroCuenta", type="string" , nullable=true)
     */
    private $numeroCuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="cbu", type="string" , nullable=true)
     */
    private $cbu;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoCuenta", type="string" , nullable=true)
     */
    private $tipoCuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="ciudadSucursal", type="string" , nullable=true)
     */
    private $ciudadSucursal;

    /**
     * @var string
     *
     * @ORM\Column(name="codigoPostal", type="string" , nullable=true)
     */
    private $codigoPostal;

     /**
     * @var string
     *
     * @ORM\Column(name="titular", type="string" , nullable=true)
     */
    private $titular;

     /**
     * @var string
     *
     * @ORM\Column(name="recipientName", type="string" , nullable=true)
     */
    private $recipientName;
     /**
     * @var string
     *
     * @ORM\Column(name="bankName", type="string" , nullable=true)
     */
    private $bankName;
     /**
     * @var string
     *
     * @ORM\Column(name="bankCity", type="string" , nullable=true)
     */
    private $bankCity;
     /**
     * @var string
     *
     * @ORM\Column(name="abaRouting", type="string" , nullable=true)
     */
    private $abaRouting;
     /**
     * @var string
     *
     * @ORM\Column(name="accountNumber", type="string" , nullable=true)
     */
    private $accountNumber;
     /**
     * @var string
     *
     * @ORM\Column(name="intermediaryBank", type="string" , nullable=true)
     */
    private $intermediaryBank;
     /**
     * @var string
     *
     * @ORM\Column(name="streetAddress", type="string" , nullable=true)
     */
    private $streetAddress;
     /**
     * @var string
     *
     * @ORM\Column(name="city", type="string" , nullable=true)
     */
    private $city;
     /**
     * @var string
     *
     * @ORM\Column(name="state", type="string" , nullable=true)
     */
    private $state;
         /**
     * @var string
     *
     * @ORM\Column(name="country", type="string" , nullable=true)
     */
    private $country;
         /**
     * @var string
     *
     * @ORM\Column(name="beneficiaryName", type="string" , nullable=true)
     */
    private $beneficiaryName;
         /**
     * @var string
     *
     * @ORM\Column(name="codigoSwift", type="string" , nullable=true)
     */
    private $codigoSwift;
         /**
     * @var string
     *
     * @ORM\Column(name="beneficiaryAccount", type="string" , nullable=true)
     */
    private $beneficiaryAccount;
         /**
     * @var string
     *
     * @ORM\Column(name="intermediaryBankSwift", type="string" , nullable=true)
     */
    private $intermediaryBankSwift;
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime $fecha
     *
     * @return Retiro
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set monto.
     *
     * @param int $monto
     *
     * @return Retiro
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto.
     *
     * @return int
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set metodo.
     *
     * @param string $metodo
     *
     * @return Retiro
     */
    public function setMetodo($metodo)
    {
        $this->metodo = $metodo;

        return $this;
    }

    /**
     * Get metodo.
     *
     * @return string
     */
    public function getMetodo()
    {
        return $this->metodo;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return Retiro
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Retiro
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dni
     *
     * @param string $dni
     *
     * @return Retiro
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set cuit
     *
     * @param string $cuit
     *
     * @return Retiro
     */
    public function setCuit($cuit)
    {
        $this->cuit = $cuit;

        return $this;
    }

    /**
     * Get cuit
     *
     * @return string
     */
    public function getCuit()
    {
        return $this->cuit;
    }

    /**
     * Set nombreBanco
     *
     * @param string $nombreBanco
     *
     * @return Retiro
     */
    public function setNombreBanco($nombreBanco)
    {
        $this->nombreBanco = $nombreBanco;

        return $this;
    }

    /**
     * Get nombreBanco
     *
     * @return string
     */
    public function getNombreBanco()
    {
        return $this->nombreBanco;
    }

    /**
     * Set numeroCuenta
     *
     * @param string $numeroCuenta
     *
     * @return Retiro
     */
    public function setNumeroCuenta($numeroCuenta)
    {
        $this->numeroCuenta = $numeroCuenta;

        return $this;
    }

    /**
     * Get numeroCuenta
     *
     * @return string
     */
    public function getNumeroCuenta()
    {
        return $this->numeroCuenta;
    }

    /**
     * Set cbu
     *
     * @param string $cbu
     *
     * @return Retiro
     */
    public function setCbu($cbu)
    {
        $this->cbu = $cbu;

        return $this;
    }

    /**
     * Get cbu
     *
     * @return string
     */
    public function getCbu()
    {
        return $this->cbu;
    }

    /**
     * Set tipoCuenta
     *
     * @param string $tipoCuenta
     *
     * @return Retiro
     */
    public function setTipoCuenta($tipoCuenta)
    {
        $this->tipoCuenta = $tipoCuenta;

        return $this;
    }

    /**
     * Get tipoCuenta
     *
     * @return string
     */
    public function getTipoCuenta()
    {
        return $this->tipoCuenta;
    }

    /**
     * Set ciudadSucursal
     *
     * @param string $ciudadSucursal
     *
     * @return Retiro
     */
    public function setCiudadSucursal($ciudadSucursal)
    {
        $this->ciudadSucursal = $ciudadSucursal;

        return $this;
    }

    /**
     * Get ciudadSucursal
     *
     * @return string
     */
    public function getCiudadSucursal()
    {
        return $this->ciudadSucursal;
    }

    /**
     * Set codigoPostal
     *
     * @param string $codigoPostal
     *
     * @return Retiro
     */
    public function setCodigoPostal($codigoPostal)
    {
        $this->codigoPostal = $codigoPostal;

        return $this;
    }

    /**
     * Get codigoPostal
     *
     * @return string
     */
    public function getCodigoPostal()
    {
        return $this->codigoPostal;
    }

    /**
     * Set titular
     *
     * @param string $titular
     *
     * @return Retiro
     */
    public function setTitular($titular)
    {
        $this->titular = $titular;

        return $this;
    }

    /**
     * Get titular
     *
     * @return string
     */
    public function getTitular()
    {
        return $this->titular;
    }

    /**
     * Set recipientName
     *
     * @param string $recipientName
     *
     * @return Retiro
     */
    public function setRecipientName($recipientName)
    {
        $this->recipientName = $recipientName;

        return $this;
    }

    /**
     * Get recipientName
     *
     * @return string
     */
    public function getRecipientName()
    {
        return $this->recipientName;
    }

    /**
     * Set bankName
     *
     * @param string $bankName
     *
     * @return Retiro
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;

        return $this;
    }

    /**
     * Get bankName
     *
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * Set bankCity
     *
     * @param string $bankCity
     *
     * @return Retiro
     */
    public function setBankCity($bankCity)
    {
        $this->bankCity = $bankCity;

        return $this;
    }

    /**
     * Get bankCity
     *
     * @return string
     */
    public function getBankCity()
    {
        return $this->bankCity;
    }

    /**
     * Set abaRouting
     *
     * @param string $abaRouting
     *
     * @return Retiro
     */
    public function setAbaRouting($abaRouting)
    {
        $this->abaRouting = $abaRouting;

        return $this;
    }

    /**
     * Get abaRouting
     *
     * @return string
     */
    public function getAbaRouting()
    {
        return $this->abaRouting;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return Retiro
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set intermediaryBank
     *
     * @param string $intermediaryBank
     *
     * @return Retiro
     */
    public function setIntermediaryBank($intermediaryBank)
    {
        $this->intermediaryBank = $intermediaryBank;

        return $this;
    }

    /**
     * Get intermediaryBank
     *
     * @return string
     */
    public function getIntermediaryBank()
    {
        return $this->intermediaryBank;
    }

    /**
     * Set streetAddress
     *
     * @param string $streetAddress
     *
     * @return Retiro
     */
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    /**
     * Get streetAddress
     *
     * @return string
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Retiro
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Retiro
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Retiro
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set beneficiaryName
     *
     * @param string $beneficiaryName
     *
     * @return Retiro
     */
    public function setBeneficiaryName($beneficiaryName)
    {
        $this->beneficiaryName = $beneficiaryName;

        return $this;
    }

    /**
     * Get beneficiaryName
     *
     * @return string
     */
    public function getBeneficiaryName()
    {
        return $this->beneficiaryName;
    }

    /**
     * Set codigoSwift
     *
     * @param string $codigoSwift
     *
     * @return Retiro
     */
    public function setCodigoSwift($codigoSwift)
    {
        $this->codigoSwift = $codigoSwift;

        return $this;
    }

    /**
     * Get codigoSwift
     *
     * @return string
     */
    public function getCodigoSwift()
    {
        return $this->codigoSwift;
    }

    /**
     * Set beneficiaryAccount
     *
     * @param string $beneficiaryAccount
     *
     * @return Retiro
     */
    public function setBeneficiaryAccount($beneficiaryAccount)
    {
        $this->beneficiaryAccount = $beneficiaryAccount;

        return $this;
    }

    /**
     * Get beneficiaryAccount
     *
     * @return string
     */
    public function getBeneficiaryAccount()
    {
        return $this->beneficiaryAccount;
    }

    /**
     * Set intermediaryBankSwift
     *
     * @param string $intermediaryBankSwift
     *
     * @return Retiro
     */
    public function setIntermediaryBankSwift($intermediaryBankSwift)
    {
        $this->intermediaryBankSwift = $intermediaryBankSwift;

        return $this;
    }

    /**
     * Get intermediaryBankSwift
     *
     * @return string
     */
    public function getIntermediaryBankSwift()
    {
        return $this->intermediaryBankSwift;
    }

    /**
     * Set evento
     *
     * @param \AppBundle\Entity\Evento $evento
     *
     * @return Retiro
     */
    public function setEvento(\AppBundle\Entity\Evento $evento = null)
    {
        $this->evento = $evento;

        return $this;
    }

    /**
     * Get evento
     *
     * @return \AppBundle\Entity\Evento
     */
    public function getEvento()
    {
        return $this->evento;
    }
}
