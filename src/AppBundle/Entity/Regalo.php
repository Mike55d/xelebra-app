<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Regalo
 *
 * @ORM\Table(name="regalo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RegaloRepository")
 */
class Regalo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="Categoria")
    * @ORM\JoinColumn(name="categoria", referencedColumnName="id" , nullable=true)
    */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="integer")
     */
    private $precio;

        /**
     * @var string
     *
     * @ORM\Column(name="sugerido", type="integer")
     */
    private $sugerido;

    /**
     * @var string
     *
     * @ORM\Column(name="lista", type="string", length=255 , nullable = true)
     */
    private $lista;

    /**
    * @ORM\ManyToOne(targetEntity="tipoEvento")
    * @ORM\JoinColumn(name="tipoEvento", referencedColumnName="id")
    */
    private $tipo;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Regalo
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Regalo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set precio
     *
     * @param string $precio
     *
     * @return Regalo
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Regalo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     *
     * @return Regalo
     */
    public function setCategoria(\AppBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \AppBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set sugerido
     *
     * @param string $sugerido
     *
     * @return Regalo
     */
    public function setSugerido($sugerido)
    {
        $this->sugerido = $sugerido;

        return $this;
    }

    /**
     * Get sugerido
     *
     * @return string
     */
    public function getSugerido()
    {
        return $this->sugerido;
    }

    /**
     * Set lista
     *
     * @param string $lista
     *
     * @return Regalo
     */
    public function setLista($lista)
    {
        $this->lista = $lista;

        return $this;
    }

    /**
     * Get lista
     *
     * @return string
     */
    public function getLista()
    {
        return $this->lista;
    }

    

    /**
     * Set tipo
     *
     * @param \AppBundle\Entity\tipoEvento $tipo
     *
     * @return Regalo
     */
    public function setTipo(\AppBundle\Entity\tipoEvento $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \AppBundle\Entity\tipoEvento
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
