<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evento
 *
 * @ORM\Table(name="evento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventoRepository")
 */
class Evento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titulo;
     /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apellido;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $nombre2;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $apellido2;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $email2;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="integer")
     */
    private $invitados;

   /**
   * @ORM\ManyToOne(targetEntity="Pais")
   * @ORM\JoinColumn(name="pais", referencedColumnName="id")
   */
    private $pais;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ciudad;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $lugarCivil;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $lugarCeremonia;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $lugarFiesta;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $lugarEvento1;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $mapa1;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $lugarEvento2;

     /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $mapa2;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $lugarEvento3;

     /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $mapa3;

    /**
    * @ORM\ManyToOne(targetEntity="Comisiones")
    * @ORM\JoinColumn(name="comision", referencedColumnName="id" , nullable=true)
    */
    private $comision;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $about;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipoLista;

    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user", referencedColumnName="id")
    */
    private $user;

    /**
    * @ORM\ManyToOne(targetEntity="tipoEvento")
    * @ORM\JoinColumn(name="tipo", referencedColumnName="id")
    */

    private $tipo;

    /**
     * @ORM\Column(type="integer", length=255 , nullable=true)
     */
    private $contribucion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="comPersonalizada", type="boolean")
     */
    private $comPersonalizada;

    /**
     * @var string
     *
     * @ORM\Column(name="fijo", type="string", length=255 , nullable=true )
     */
    private $fijo;

        /**
     * @var string
     *
     * @ORM\Column(name="fijoNovio", type="string", length=255 , nullable=true)
     */
    private $fijoNovio;

        /**
     * @var string
     *
     * @ORM\Column(name="fijoInvitado", type="string", length=255 , nullable=true)
     */
    private $fijoInvitado;

        /**
     * @var string
     *
     * @ORM\Column(name="impuestoPais", type="string", length=255 , nullable=true)
     */
    private $impuestoPais;

    /**
     * @var string
     *
     * @ORM\Column(name="porcentNovio", type="string", length=255 , nullable=true)
     */
    private $porcentNovio;

    /**
     * @var string
     *
     * @ORM\Column(name="porcentInvitado", type="string", length=255 , nullable=true)
     */
    private $porcentInvitado;

/**
     * @var string
     *
     * @ORM\Column(name="tipoCobro", type="string", length=255 , nullable=true)
     */
    private $tipoCobro;

    /**
     * @var integer
     *
     * @ORM\Column(name="saldo", type="integer", length=255 )
     */
    private $saldo;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Evento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return Evento
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Evento
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set nombre2
     *
     * @param string $nombre2
     *
     * @return Evento
     */
    public function setNombre2($nombre2)
    {
        $this->nombre2 = $nombre2;

        return $this;
    }

    /**
     * Get nombre2
     *
     * @return string
     */
    public function getNombre2()
    {
        return $this->nombre2;
    }

    /**
     * Set apellido2
     *
     * @param string $apellido2
     *
     * @return Evento
     */
    public function setApellido2($apellido2)
    {
        $this->apellido2 = $apellido2;

        return $this;
    }

    /**
     * Get apellido2
     *
     * @return string
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }

    /**
     * Set email2
     *
     * @param string $email2
     *
     * @return Evento
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;

        return $this;
    }

    /**
     * Get email2
     *
     * @return string
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Evento
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Evento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set invitados
     *
     * @param integer $invitados
     *
     * @return Evento
     */
    public function setInvitados($invitados)
    {
        $this->invitados = $invitados;

        return $this;
    }

    /**
     * Get invitados
     *
     * @return integer
     */
    public function getInvitados()
    {
        return $this->invitados;
    }


    /**
     * Set ciudad
     *
     * @param string $ciudad
     *
     * @return Evento
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad
     *
     * @return string
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set lugarCivil
     *
     * @param string $lugarCivil
     *
     * @return Evento
     */
    public function setLugarCivil($lugarCivil)
    {
        $this->lugarCivil = $lugarCivil;

        return $this;
    }

    /**
     * Get lugarCivil
     *
     * @return string
     */
    public function getLugarCivil()
    {
        return $this->lugarCivil;
    }

    /**
     * Set lugarCeremonia
     *
     * @param string $lugarCeremonia
     *
     * @return Evento
     */
    public function setLugarCeremonia($lugarCeremonia)
    {
        $this->lugarCeremonia = $lugarCeremonia;

        return $this;
    }

    /**
     * Get lugarCeremonia
     *
     * @return string
     */
    public function getLugarCeremonia()
    {
        return $this->lugarCeremonia;
    }

    /**
     * Set lugarFiesta
     *
     * @param string $lugarFiesta
     *
     * @return Evento
     */
    public function setLugarFiesta($lugarFiesta)
    {
        $this->lugarFiesta = $lugarFiesta;

        return $this;
    }

    /**
     * Get lugarFiesta
     *
     * @return string
     */
    public function getLugarFiesta()
    {
        return $this->lugarFiesta;
    }

    /**
     * Set about
     *
     * @param string $about
     *
     * @return Evento
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Evento
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Evento
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set pais
     *
     * @param \AppBundle\Entity\Pais $pais
     *
     * @return Evento
     */
    public function setPais(\AppBundle\Entity\Pais $pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return \AppBundle\Entity\Pais
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set tipoLista.
     *
     * @param string $tipoLista
     *
     * @return Evento
     */
    public function setTipoLista($tipoLista)
    {
        $this->tipoLista = $tipoLista;

        return $this;
    }

    /**
     * Get tipoLista.
     *
     * @return string
     */
    public function getTipoLista()
    {
        return $this->tipoLista;
    }

    /**
     * Set tipo.
     *
     * @param \AppBundle\Entity\tipoEvento|null $tipo
     *
     * @return Evento
     */
    public function setTipo(\AppBundle\Entity\tipoEvento $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo.
     *
     * @return \AppBundle\Entity\tipoEvento|null
     */
    public function getTipo()
    {
        return $this->tipo;
    }


    /**
     * Set contribucion.
     *
     * @param int $contribucion
     *
     * @return Evento
     */
    public function setContribucion($contribucion)
    {
        $this->contribucion = $contribucion;

        return $this;
    }

    /**
     * Get contribucion.
     *
     * @return int
     */
    public function getContribucion()
    {
        return $this->contribucion;
    }

    /**
     * Set lugarEvento1.
     *
     * @param string|null $lugarEvento1
     *
     * @return Evento
     */
    public function setLugarEvento1($lugarEvento1 = null)
    {
        $this->lugarEvento1 = $lugarEvento1;

        return $this;
    }

    /**
     * Get lugarEvento1.
     *
     * @return string|null
     */
    public function getLugarEvento1()
    {
        return $this->lugarEvento1;
    }

    /**
     * Set mapa1.
     *
     * @param string|null $mapa1
     *
     * @return Evento
     */
    public function setMapa1($mapa1 = null)
    {
        $this->mapa1 = $mapa1;

        return $this;
    }

    /**
     * Get mapa1.
     *
     * @return string|null
     */
    public function getMapa1()
    {
        return $this->mapa1;
    }

    /**
     * Set lugarEvento2.
     *
     * @param string|null $lugarEvento2
     *
     * @return Evento
     */
    public function setLugarEvento2($lugarEvento2 = null)
    {
        $this->lugarEvento2 = $lugarEvento2;

        return $this;
    }

    /**
     * Get lugarEvento2.
     *
     * @return string|null
     */
    public function getLugarEvento2()
    {
        return $this->lugarEvento2;
    }

    /**
     * Set mapa2.
     *
     * @param string|null $mapa2
     *
     * @return Evento
     */
    public function setMapa2($mapa2 = null)
    {
        $this->mapa2 = $mapa2;

        return $this;
    }

    /**
     * Get mapa2.
     *
     * @return string|null
     */
    public function getMapa2()
    {
        return $this->mapa2;
    }

    /**
     * Set lugarEvento3.
     *
     * @param string|null $lugarEvento3
     *
     * @return Evento
     */
    public function setLugarEvento3($lugarEvento3 = null)
    {
        $this->lugarEvento3 = $lugarEvento3;

        return $this;
    }

    /**
     * Get lugarEvento3.
     *
     * @return string|null
     */
    public function getLugarEvento3()
    {
        return $this->lugarEvento3;
    }

    /**
     * Set mapa3.
     *
     * @param string|null $mapa3
     *
     * @return Evento
     */
    public function setMapa3($mapa3 = null)
    {
        $this->mapa3 = $mapa3;

        return $this;
    }

    /**
     * Get mapa3.
     *
     * @return string|null
     */
    public function getMapa3()
    {
        return $this->mapa3;
    }

    /**
     * Set comision
     *
     * @param \AppBundle\Entity\Comisiones $comision
     *
     * @return Evento
     */
    public function setComision(\AppBundle\Entity\Comisiones $comision = null)
    {
        $this->comision = $comision;

        return $this;
    }

    /**
     * Get comision
     *
     * @return \AppBundle\Entity\Comisiones
     */
    public function getComision()
    {
        return $this->comision;
    }

    /**
     * Set comPersonalizada
     *
     * @param boolean $comPersonalizada
     *
     * @return Evento
     */
    public function setComPersonalizada($comPersonalizada)
    {
        $this->comPersonalizada = $comPersonalizada;

        return $this;
    }

    /**
     * Get comPersonalizada
     *
     * @return boolean
     */
    public function getComPersonalizada()
    {
        return $this->comPersonalizada;
    }

    /**
     * Set fijo
     *
     * @param string $fijo
     *
     * @return Evento
     */
    public function setFijo($fijo)
    {
        $this->fijo = $fijo;

        return $this;
    }

    /**
     * Get fijo
     *
     * @return string
     */
    public function getFijo()
    {
        return $this->fijo;
    }

    /**
     * Set fijoNovio
     *
     * @param string $fijoNovio
     *
     * @return Evento
     */
    public function setFijoNovio($fijoNovio)
    {
        $this->fijoNovio = $fijoNovio;

        return $this;
    }

    /**
     * Get fijoNovio
     *
     * @return string
     */
    public function getFijoNovio()
    {
        return $this->fijoNovio;
    }

    /**
     * Set fijoInvitado
     *
     * @param string $fijoInvitado
     *
     * @return Evento
     */
    public function setFijoInvitado($fijoInvitado)
    {
        $this->fijoInvitado = $fijoInvitado;

        return $this;
    }

    /**
     * Get fijoInvitado
     *
     * @return string
     */
    public function getFijoInvitado()
    {
        return $this->fijoInvitado;
    }

    /**
     * Set impuestoPais
     *
     * @param string $impuestoPais
     *
     * @return Evento
     */
    public function setImpuestoPais($impuestoPais)
    {
        $this->impuestoPais = $impuestoPais;

        return $this;
    }

    /**
     * Get impuestoPais
     *
     * @return string
     */
    public function getImpuestoPais()
    {
        return $this->impuestoPais;
    }

    /**
     * Set porcentNovio
     *
     * @param string $porcentNovio
     *
     * @return Evento
     */
    public function setPorcentNovio($porcentNovio)
    {
        $this->porcentNovio = $porcentNovio;

        return $this;
    }

    /**
     * Get porcentNovio
     *
     * @return string
     */
    public function getPorcentNovio()
    {
        return $this->porcentNovio;
    }

    /**
     * Set porcentInvitado
     *
     * @param string $porcentInvitado
     *
     * @return Evento
     */
    public function setPorcentInvitado($porcentInvitado)
    {
        $this->porcentInvitado = $porcentInvitado;

        return $this;
    }

    /**
     * Get porcentInvitado
     *
     * @return string
     */
    public function getPorcentInvitado()
    {
        return $this->porcentInvitado;
    }

    /**
     * Set tipoCobro
     *
     * @param string $tipoCobro
     *
     * @return Evento
     */
    public function setTipoCobro($tipoCobro)
    {
        $this->tipoCobro = $tipoCobro;

        return $this;
    }

    /**
     * Get tipoCobro
     *
     * @return string
     */
    public function getTipoCobro()
    {
        return $this->tipoCobro;
    }

    /**
     * Set saldo
     *
     * @param integer $saldo
     *
     * @return Evento
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;

        return $this;
    }

    /**
     * Get saldo
     *
     * @return integer
     */
    public function getSaldo()
    {
        return $this->saldo;
    }
}
